/****************************************************************************
**  ircsession.h
**
**  Copyright information
**
**      Copyright (C) 2011-2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef IRCSESSION_H
#define IRCSESSION_H

#include "network/localsocket.h"
#include "clientsession.h"
#include <QtCore/QObject>
#include <QtCore/QStringList>

class IrcSession;

typedef void IrcSessionHandleMethod(QString, NetworkList *, IrcSession *);
typedef QMap<QString, IrcSessionHandleMethod *> IrcSessionHandleMethodList;

/**
 *  This class is for handling IRC session.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2013-08-28
 */
class IrcSession : public QObject
{
    Q_OBJECT
public:
    IrcSession(LocalSocket *socket, QObject *parent = 0);

    void write(QString line);
    void insertClientSession(ClientSession *clientSession);

    static void set();

private:
    bool          isRegistered;
    int           uid;
    int           scrollback;
    Settings      settings;
    LocalSocket   *socket;
    NetworkList   networks;
    ClientSession *completeLineReplyTarget;

    void setScrollback(int scrollback);
    void registerSession(QString line, bool force = false);

    static IrcSessionHandleMethodList handleMethods;

    static void newSettingValue(QString line, NetworkList *networks, IrcSession *ircSession);
    static void setColors(QString line, NetworkList *networks, IrcSession *ircSession);
    static void insertNetwork(QString line, NetworkList *networks, IrcSession *ircSession);
    static void insertChat(QString line, NetworkList *networks, IrcSession *ircSession);
    static void insertNick(QString line, NetworkList *networks, IrcSession *ircSession);
    static void insertNicks(QString line, NetworkList *networks, IrcSession *ircSession);
    static void removeNetwork(QString line, NetworkList *networks, IrcSession *ircSession);
    static void removeChat(QString line, NetworkList *networks, IrcSession *ircSession);
    static void removeNick(QString line, NetworkList *networks, IrcSession *ircSession);
    static void changeNick(QString line, NetworkList *networks, IrcSession *ircSession);
    static void changeNickMode(QString line, NetworkList *networks, IrcSession *ircSession);
    static void renameChat(QString line, NetworkList *networks, IrcSession *ircSession);
    static void newMsg(QString line, NetworkList *networks, IrcSession *ircSession);
    static void newHighlightMsg(QString line, NetworkList *networks, IrcSession *ircSession);
    static void newOwnMsg(QString line, NetworkList *networks, IrcSession *ircSession);
    static void newDccFile(QString line, NetworkList *networks, IrcSession *ircSession);
    static void closeDccFile(QString line, NetworkList *networks, IrcSession *ircSession);
    static void lineCompleted(QString line, NetworkList *networks, IrcSession *ircSession);

Q_SIGNALS:
    void networkAdded(QString network);
    void chatAdded(QString network, QString chat);
    void nickAdded(QString network, QString chat, QString nick);
    void nicksAdded(QString network, QString chat, QStringList nicks);
    void networkRemoved(QString network);
    void chatRemoved(QString network, QString chat);
    void nickRemoved(QString network, QString chat, QString nick);
    void nickChanged(QString network, QString chat, QString oldNick, QString newNick);
    void nickModeChanged(QString network, QString chat, QString nick, QString mode);
    void chatRenamed(QString network, QString oldChatName, QString newChatName);
    void msgAdded(QString network, QString chat, QString line);
    void highlightMsgAdded(QString network, QString chat, QString line);
    void ownMsgAdded(QString network, QString chat, QString line);
    void incommingDccFile(QString network, QString sender, QString file);
    void closeDccFile(QString network, QString sender, QString file);
    void linesRemoved(QString network, QString chat);

public Q_SLOTS:
    void settingsChanged();

private Q_SLOTS:
    void readData();
    void disconnected();
    void bytesWritten();
    void writeLine(QString network, QString chat, QString line);
    void completeLine(QString network, QString chat, QString line, int pos);
    void dccFileReply(QString network, QString sender, QString file, bool get);
    void quitNetwork(QString network);
    void closeChat(QString network, QString chat);
    void query(QString network, QString nick);
};

#endif // IRCSESSION_H
