/****************************************************************************
**  misc.cpp
**
**  Copyright information
**
**      Copyright (C) 2011-2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "misc.h"
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

QFile *Misc::logFile;
QFile *Misc::pidFile;

/**
 * Set.
 */
void Misc::set ()
{
    logFile = new QFile(LOG_FILE);
    pidFile = new QFile(PID_FILE);
}

/**
 * Write log message.
 *
 * @param text Log message.
 */
void Misc::log (QString text)
{
    text = text + "\n";
    logFile->open(QIODevice::Append | QIODevice::Text);
    logFile->write(text.toUtf8());
    logFile->flush();
    logFile->close();
}

/**
 * Write pid into file.
 */
void Misc::writePid ()
{
    QString pid = QString::number(getpid());

    if ( !pidFile->open(QIODevice::Append) )
    {
        Misc::log("Cannot open file " + QString(PID_FILE) + "!");
        exit(1);
    }

    if ( lockf(pidFile->handle(),F_TLOCK,0) == -1 )
    {
        Misc::log("Cannot lock file " + QString(PID_FILE) + "!");
        exit(1);
    }

    pidFile->resize(0);
    pidFile->write(pid.toUtf8());
    pidFile->flush();
}

/**
 * Get nanoseconds passed since 1970-01-01T00:00:00.000.
 *
 * @return Nanoseconds passed since 1970-01-01T00:00:00.000.
 */
qint64 Misc::getEpochNs ()
{
    timespec time;

    clock_gettime(CLOCK_REALTIME, &time);

    return static_cast<qint64>(time.tv_sec) * 1000000000LL + static_cast<qint64>(time.tv_nsec);
}
