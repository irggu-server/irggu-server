/****************************************************************************
**  clientpriority.cpp
**
**  Copyright information
**
**      Copyright (C) 2012-2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "clientpriority.h"
#include "clientsession.h"
#include "misc.h"

/**
 * Constructor.
 */
ClientPriority::ClientPriority ()
{
    priority = 9;
}

/**
 * Add client session.
 *
 * @param clientSession Client session.
 */
void ClientPriority::addClientSession (ClientSession *clientSession)
{
    clients.insert(clientSession, 9);
}

/**
 * Remove client session.
 *
 * @param clientSession Client session.
 */
void ClientPriority::removeClientSession (ClientSession *clientSession)
{
    clients.remove(clientSession);
}

/**
 * Set client idle status.
 *
 * @param idle          Client idle status.
 * @param priority      Client priority.
 * @param clientSession Client session.
 */
void ClientPriority::setIdle (bool idle, int priority, ClientSession *clientSession)
{
    if ( idle )
    {
        if ( !idlers.contains(clientSession) )
        {
            idlers.append(clientSession);
        }
    }
    else
    {
        if ( idlers.contains(clientSession) )
        {
            idlers.removeAt(idlers.indexOf(clientSession));
        }
    }

    if ( priority < this->priority )
    {
        this->priority = priority;
    }

    clients.insert(clientSession, priority);
}

/**
 * Returns true if client is high priority.
 *
 * @param clientSession Client session.
 */
bool ClientPriority::isHighPriority (ClientSession *clientSession)
{
    bool highPriority = false;

    if ( clients.count() == idlers.count() )
    {
        if ( clients.value(clientSession) == priority )
        {
            highPriority = true;
        }
    }

    return highPriority;
}
