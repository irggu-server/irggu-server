/****************************************************************************
**  loginsession.h
**
**  Copyright information
**
**      Copyright (C) 2011-2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef LOGINSESSION_H
#define LOGINSESSION_H

#include "network/tcpsocket.h"
#include <QtCore/QObject>

/**
 *  This class is for handling client login session.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2013-08-28
 */
class LoginSession : public QObject
{
    Q_OBJECT
public:
    LoginSession(TcpSocket *socket, QObject *parent = 0);

private:
    TcpSocket *socket;

    void write(QString line);

private Q_SLOTS:
    void connected();
    void disconnected();
    void readData();
    void bytesWritten();
};

#endif // LOGINSESSION_H
