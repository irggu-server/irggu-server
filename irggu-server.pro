#-------------------------------------------------
#
# Project created by QtCreator 2011-05-03T01:27:02
#
#-------------------------------------------------

QT += core \
   network

QT -= gui

TARGET = irggu-server
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    irgguserver.cpp \
    misc.cpp \
    config.cpp \
    ircsession.cpp \
    manager.cpp \
    clientsession.cpp \
    loginsession.cpp \
    clientpriority.cpp \
    network/tcpserver.cpp \
    network/tcpsocket.cpp \
    network/localserver.cpp \
    network/localsocket.cpp

HEADERS += \
    irgguserver.h \
    misc.h \
    config.h \
    ircsession.h \
    manager.h \
    clientsession.h \
    loginsession.h \
    clientpriority.h \
    network/tcpserver.h \
    network/tcpsocket.h \
    protocol/websocket.h \
    protocol/irggu.h \
    network/localserver.h \
    network/localsocket.h

OTHER_FILES += \
    protocol.txt \
    CMakeLists.txt

#DEFINES += PID_FILE=\\\"/var/run/irggu/pid\\\" \
#    SOCK_FILE=\\\"/var/run/irggu/sock\\\" \
#    LOG_FILE=\\\"/var/log/irggu.lock\\\" \
#    CONF_FILE=\\\"/tmp/irggu.cfg\\\"

DEFINES += PID_FILE=\\\"/tmp/irggu.pid\\\" \
    SOCK_FILE=\\\"/tmp/irggu.sock\\\" \
    LOG_FILE=\\\"/tmp/irggu.log\\\" \
    CONF_FILE=\\\"/tmp/irggu.conf\\\"

QMAKE_CXXFLAGS += -std=c++11
