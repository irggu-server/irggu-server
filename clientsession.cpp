/****************************************************************************
**  clientsession.cpp
**
**  Copyright information
**
**      Copyright (C) 2011-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "protocol/irggu.h"
#include "clientsession.h"
#include "manager.h"
#include "misc.h"
#include <QDateTime>
#include <QRegularExpression>

ClientPriority                ClientSession::clientPriority;
ClientSessionHandleMethodList ClientSession::handleMethods;

/**
 * Constructor.
 *
 * @param username  Username.
 * @param sessionId Session ID.
 * @param *parent   Pointer to parent.
 */
ClientSession::ClientSession (QString username, int sessionID, QObject *parent) : QObject(parent)
{
    srand(time(NULL));

    this->socket           = NULL;
    this->username         = username;
    this->lastDisconnected = 0;
    this->priority         = 0;
    this->sessionID        = sessionID;
    this->connectionID     = rand() % 8999999 + 1000000;
    this->temporary        = false;
    this->alertsEnabled    = true;
    this->sessionStarted   = false;
    this->sessionStopped   = false;
    this->idle             = false;

    clientPriority.addClientSession(this);
}

/**
 * Destructor.
 */
ClientSession::~ClientSession ()
{
    clientPriority.removeClientSession(this);
}

/**
 * Set socket for client session.
 *
 * @param socket Socket.
 */
void ClientSession::setSocket (TcpSocket *socket)
{
    if ( this->socket )
    {
        disconnect(this->socket);

        this->socket->disconnectFromClient();
        this->socket->deleteLater();
    }

    this->sessionStarted = false;
    this->sessionStopped = false;

    this->socket = socket;
    this->socket->setParent(this);

    connect(socket, SIGNAL(readyRead()), this, SLOT(readData()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
    connect(socket, SIGNAL(bytesWritten(qint64)), this, SLOT(bytesWritten(qint64)));

    QString command = IrGGu_Write::loginSession;

    command.replace("<result>", "0");
    command.replace("<sessionID>", QString::number(this->sessionID));
    command.replace("<connectionID>", QString::number(this->connectionID));

    write(command, false, false);
}

/**
 * Set scrollback.
 *
 * @param scrollback Scrollback.
 */
void ClientSession::setScrollback (int *scrollback)
{
    this->scrollback = scrollback;
}

/**
 * Set colors.
 *
 * @param colors List of colors.
 */
void ClientSession::setColors (QMap<QString, QString> colors)
{
    QMap<QString, QString> colorsList;

    colorsList.insert("<foreground>", colors.value("foreground"));
    colorsList.insert("<local_0>", colors.value("local_0"));
    colorsList.insert("<local_1>", colors.value("local_1"));
    colorsList.insert("<local_2>", colors.value("local_2"));
    colorsList.insert("<local_3>", colors.value("local_3"));
    colorsList.insert("<local_4>", colors.value("local_4"));
    colorsList.insert("<local_5>", colors.value("local_5"));
    colorsList.insert("<local_6>", colors.value("local_6"));
    colorsList.insert("<local_7>", colors.value("local_7"));
    colorsList.insert("<local_8>", colors.value("local_8"));
    colorsList.insert("<local_9>", colors.value("local_9"));
    colorsList.insert("<local_10>", colors.value("local_10"));
    colorsList.insert("<local_11>", colors.value("local_11"));
    colorsList.insert("<local_12>", colors.value("local_12"));
    colorsList.insert("<local_13>", colors.value("local_13"));
    colorsList.insert("<local_14>", colors.value("local_14"));
    colorsList.insert("<local_15>", colors.value("local_15"));
    colorsList.insert("<mirc_0>", colors.value("mirc_0"));
    colorsList.insert("<mirc_1>", colors.value("mirc_1"));
    colorsList.insert("<mirc_2>", colors.value("mirc_2"));
    colorsList.insert("<mirc_3>", colors.value("mirc_3"));
    colorsList.insert("<mirc_4>", colors.value("mirc_4"));
    colorsList.insert("<mirc_5>", colors.value("mirc_5"));
    colorsList.insert("<mirc_6>", colors.value("mirc_6"));
    colorsList.insert("<mirc_7>", colors.value("mirc_7"));
    colorsList.insert("<mirc_8>", colors.value("mirc_8"));
    colorsList.insert("<mirc_9>", colors.value("mirc_9"));
    colorsList.insert("<mirc_10>", colors.value("mirc_10"));
    colorsList.insert("<mirc_11>", colors.value("mirc_11"));
    colorsList.insert("<mirc_12>", colors.value("mirc_12"));
    colorsList.insert("<mirc_13>", colors.value("mirc_13"));
    colorsList.insert("<mirc_14>", colors.value("mirc_14"));
    colorsList.insert("<mirc_15>", colors.value("mirc_15"));

    settings.insert("colors", colorsList);
}


/**
 * Set timestamp settings.
 *
 * @param timestamp Timestamp settings.
 */
void ClientSession::setTimestamp (QMap<QString, QString> timestamp)
{
    settings.insert("timestamp", timestamp);
}

/**
 * Insert IRC session data.
 *
 * @param networks List of Networks that contains all IRC session Data.
 */
void ClientSession::insertIrcSessionData (NetworkList networks)
{
    QMapIterator<QString, Network *> networksIt(networks);
    Network *network;
    Chat *chat;
    ChatChanges *chatChanges;
    QString networkName;
    QString chatName;
    QString nicks;
    QString command;

    while ( networksIt.hasNext() )
    {
        networksIt.next();
        network     = networksIt.value();
        networkName = networksIt.key();

        command = IrGGu_Write::insertNetwork;

        command.replace("<network>", networkName);

        commands.append(command);

        QMapIterator<QString, Chat *> chatsIt(network->chats);
        QMap<QString, ChatChanges *> chatChangesList;

        while ( chatsIt.hasNext() )
        {
            chatsIt.next();
            chat     = chatsIt.value();
            chatName = chatsIt.key();

            if ( chatName != "(server)" )
            {
                command = IrGGu_Write::insertChat;

                command.replace("<network>", networkName);
                command.replace("<chat>", chatName);

                commands.append(command);
            }

            chatChanges         = new ChatChanges;
            chatChanges->append = chat->chat;

            chatChangesList.insert(chatName, chatChanges);


            if ( !chat->nickList.isEmpty() )
            {
                QStringListIterator nickListIt(chat->nickList);

                while ( nickListIt.hasNext() )
                {
                    nicks += nickListIt.next() + " ";
                }

                command = IrGGu_Write::insertNicks;

                command.replace("<network>", networksIt.key());
                command.replace("<chat>", chatsIt.key());
                command.replace("<nicks>", nicks.trimmed());

                commands.append(command);

                nicks.clear();
            }
        }

        allChatChanges.insert(networkName, chatChangesList);
    }
}

/**
 * Send completed line to client.
 *
 * @param network Network.
 * @param chat    Chat.
 * @param line    Completed line.
 */
void ClientSession::completedLine (QString network, QString chat, QString line)
{
    if ( socket && sessionStarted && !idle )
    {
        QString command = IrGGu_Write::lineCompleted;

        command.replace("<network>", network);
        command.replace("<chat>", chat);
        command.replace("<line>", line);

        write(command);
    }
}

/**
 * Return time when last disconnection happened.
 *
 * @return Time when last disconnection happened.
 */
qint64 ClientSession::getLastDisconnected ()
{
    return lastDisconnected;
}

/**
 * Return true if client is the same which was this session created for.
 *
 * @param connectionID Connection ID.
 * @return True if client is the same which was this session created for.
 */
bool ClientSession::isSameClient (int connectionID)
{
    return this->connectionID == connectionID;
}

/**
 * Set handle methods list.
 */
void ClientSession::set ()
{
    handleMethods.insert(IrGGu_Commands::newLine, &newLine);
    handleMethods.insert(IrGGu_Commands::newCompleteLine, &newCompleteLine);
    handleMethods.insert(IrGGu_Commands::acceptDccFile, &newdccFileReply);
    handleMethods.insert(IrGGu_Commands::doNotAcceptDccFile, &newdccFileReply);
    handleMethods.insert(IrGGu_Commands::newSettingValue, &newSettingValue);
    handleMethods.insert(IrGGu_Commands::setColors, &setColors);
    handleMethods.insert(IrGGu_Commands::setIdle, &setIdle);
    handleMethods.insert(IrGGu_Commands::quitNetwork, &quitNetwork);
    handleMethods.insert(IrGGu_Commands::closeChat, &closeChat);
    handleMethods.insert(IrGGu_Commands::query, &query);
}

/**
 * Write line to client.
 *
 * @param line        Line to write.
 * @param writeBuffer Specifies that should the line to be written from buffer.
 * @param last        Specifies that should the line to be added as last in buffer.
 */
void ClientSession::write (QString line, bool writeBuffer, bool last)
{
    line += "\n";

    BufferedLine *bufferedLine = new BufferedLine();

    bufferedLine->bytes = socket->write(line.toUtf8());
    bufferedLine->line  = line;
    bufferedLine->write = writeBuffer;

    if ( last )
    {
        buffer.append(bufferedLine);
    }
    else
    {
        buffer.prepend(bufferedLine);
    }
}

/**
 * Write line buffer to client.
 */
void ClientSession::writeBuffer ()
{
    QMutableListIterator<BufferedLine *> bufferIt(buffer);
    BufferedLine                         *bufferedLine;

    while ( bufferIt.hasNext() )
    {
        bufferedLine = bufferIt.next();

        if ( bufferedLine->write )
        {
            socket->write(bufferedLine->line.toUtf8());
        }
        else
        {
            bufferIt.remove();
        }
    }
}

/**
 * Start client session.
 */
void ClientSession::startSession ()
{
    sessionStarted = true;

    writeBuffer();

    QStringListIterator commandsIt(commands);

    while ( commandsIt.hasNext() )
    {
        write(commandsIt.next());
    }

    QMapIterator<QString, QMap<QString, ChatChanges *> > allChatChangesIt(allChatChanges);
    QMap<QString, ChatChanges *> chatChangesList;
    QMap<QString, QString> timestampSettings = this->settings.value("timestamp");
    QString networkName;
    QString chatName;
    QString dateTimeFormat = timestampSettings.value("format");
    QString msg;
    QString timestamp;
    QString command;
    ChatChanges *chatChanges;
    bool timestampEnabled = timestampSettings.value("state") == "enabled";

    while ( allChatChangesIt.hasNext() )
    {
        allChatChangesIt.next();
        networkName     = allChatChangesIt.key();
        chatChangesList = allChatChangesIt.value();

        QMapIterator<QString, ChatChanges *> chatChangesListIt(chatChangesList);

        while ( chatChangesListIt.hasNext() )
        {
            chatChangesListIt.next();
            chatName    = chatChangesListIt.key();
            chatChanges = chatChangesListIt.value();

            QMapIterator<qint64, QString> appendIt(chatChanges->append);

            while ( appendIt.hasNext() )
            {
                appendIt.next();

                if ( timestampEnabled )
                {
                    QDateTime time = QDateTime::fromMSecsSinceEpoch(appendIt.key() / 1000000LL);
                    timestamp      = time.toString(dateTimeFormat);
                }

                msg = formatChatLine(settings.value("colors"), timestamp, appendIt.value());

                command = IrGGu_Write::appendMsg;

                command.replace("<network>", networkName);
                command.replace("<chat>", chatName);
                command.replace("<msg>", msg);

                write(command);
            }
        }

        qDeleteAll(chatChangesList.begin(), chatChangesList.end());
    }

    QStringListIterator dccFilesIt(dccFiles.keys());

    while ( dccFilesIt.hasNext() )
    {
        write(dccFilesIt.next());
    }

    allChatChanges.clear();
    commands.clear();
    dccFiles.clear();
}

/**
 * Handle line to be written to IRC from the client.
 *
 * @param line           Line to write.
 * @param *clientSession Client session that received the line.
 */
void ClientSession::newLine (QString line, ClientSession *clientSession)
{
    QRegularExpression re(IrGGu_Receive::newLine);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString networkName = rem.captured("network");
        QString chatName    = rem.captured("chat");
        QString text        = rem.captured("line");

        Q_EMIT clientSession->writeLine(networkName, chatName, text);
    }
}

/**
 * Handle line that need to be completed from the client.
 *
 * @param line           Line to completed.
 * @param *clientSession Client session that received the line.
 */
void ClientSession::newCompleteLine (QString line, ClientSession *clientSession)
{
    QRegularExpression re(IrGGu_Receive::newCompleteLine);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString networkName = rem.captured("network");
        QString chatName    = rem.captured("chat");
        QString text        = rem.captured("line");
        int pos             = rem.captured("pos").toInt();

        Q_EMIT clientSession->completeLine(networkName, chatName, text, pos);
    }
}

/**
 * Handle dcc file send reply from the client.
 *
 * @param line           Line that contains the reply.
 * @param *clientSession Client session that received the reply.
 */
void ClientSession::newdccFileReply (QString line, ClientSession *clientSession)
{
    QRegularExpression aRe(IrGGu_Receive::acceptDccFile);
    QRegularExpression dRe(IrGGu_Receive::doNotAcceptDccFile);
    QRegularExpressionMatch aRem = aRe.match(line);
    QRegularExpressionMatch dRem = dRe.match(line);

    if ( aRem.hasMatch() )
    {
        QString networkName = aRem.captured("network");
        QString chatName    = aRem.captured("sender");
        QString fileName    = aRem.captured("file");

        Q_EMIT clientSession->dccFileReply(networkName, chatName, fileName, true);
    }
    else if ( dRem.hasMatch() )
    {
        QString networkName = dRem.captured("network");
        QString chatName    = dRem.captured("sender");
        QString fileName    = dRem.captured("file");

        Q_EMIT clientSession->dccFileReply(networkName, chatName, fileName, false);
    }
}

/**
 * Handle setting change from the client.
 *
 * @param line           Line that contains the setting and its value.
 * @param *clientSession Client session that received the setting change.
 */
void ClientSession::newSettingValue (QString line, ClientSession *clientSession)
{
    QRegularExpression re(IrGGu_Receive::newSettingValue);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString category = rem.captured("category");
        QString setting  = rem.captured("setting");
        QString value    = rem.captured("value");



        if ( clientSession->settings.contains(category) )
        {
            QMap<QString, QString> settingsList = clientSession->settings.value(category);

            if ( settingsList.contains(setting) )
            {
                settingsList.insert(setting, value);
                clientSession->settings.insert(category, settingsList);
            }
        }
    }
}

/**
 * Handle colors change from the client.
 *
 * @param line           Line that contains colors.
 * @param *clientSession Client session that received the colors change.
 */
void ClientSession::setColors (QString line, ClientSession *clientSession)
{
    QRegularExpression re(IrGGu_Receive::setColors);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QStringList colors = rem.captured("colors").split(QRegularExpression("\\s"));

        if ( colors.length() == 33 )
        {
            QMap<QString, QString> colorsList;

            colorsList.insert("<foreground>", colors.at(0));
            colorsList.insert("<local_0>", colors.at(1));
            colorsList.insert("<local_1>", colors.at(2));
            colorsList.insert("<local_2>", colors.at(3));
            colorsList.insert("<local_3>", colors.at(4));
            colorsList.insert("<local_4>", colors.at(5));
            colorsList.insert("<local_5>", colors.at(6));
            colorsList.insert("<local_6>", colors.at(7));
            colorsList.insert("<local_7>", colors.at(8));
            colorsList.insert("<local_8>", colors.at(9));
            colorsList.insert("<local_9>", colors.at(10));
            colorsList.insert("<local_10>", colors.at(11));
            colorsList.insert("<local_11>", colors.at(12));
            colorsList.insert("<local_12>", colors.at(13));
            colorsList.insert("<local_13>", colors.at(14));
            colorsList.insert("<local_14>", colors.at(15));
            colorsList.insert("<local_15>", colors.at(16));
            colorsList.insert("<mirc_0>", colors.at(17));
            colorsList.insert("<mirc_1>", colors.at(18));
            colorsList.insert("<mirc_2>", colors.at(19));
            colorsList.insert("<mirc_3>", colors.at(20));
            colorsList.insert("<mirc_4>", colors.at(21));
            colorsList.insert("<mirc_5>", colors.at(22));
            colorsList.insert("<mirc_6>", colors.at(23));
            colorsList.insert("<mirc_7>", colors.at(24));
            colorsList.insert("<mirc_8>", colors.at(25));
            colorsList.insert("<mirc_9>", colors.at(26));
            colorsList.insert("<mirc_10>",colors.at(27));
            colorsList.insert("<mirc_11>", colors.at(28));
            colorsList.insert("<mirc_12>", colors.at(29));
            colorsList.insert("<mirc_13>", colors.at(30));
            colorsList.insert("<mirc_14>", colors.at(31));
            colorsList.insert("<mirc_15>", colors.at(32));

            clientSession->settings.insert("colors", colorsList);
        }
    }
}

/**
 * Handle new idle state from the client.
 *
 * @param line           Line that contains the new idle state.
 * @param *clientSession Client session that received the new dile state.
 */
void ClientSession::setIdle (QString line, ClientSession *clientSession)
{
    QRegularExpression re(IrGGu_Receive::setIdle);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        bool idle     = rem.captured("idle") == "true";
        bool alerts   = rem.captured("alerts") == "true";
        int  priority = rem.captured("priority").toInt();

        clientSession->idle          = idle;
        clientSession->alertsEnabled = alerts;

        if ( !idle )
        {
            clientSession->startSession();
        }

        clientPriority.setIdle(idle, priority, clientSession);
    }

}

/**
 * Handle quit network request from the client.
 *
 * @param line           Line that contains quit network request.
 * @param *clientSession Client session that received the quit network request.
 */
void ClientSession::quitNetwork (QString line, ClientSession *clientSession)
{
    QRegularExpression re(IrGGu_Receive::quitNetwork);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString network = rem.captured("network");

        Q_EMIT clientSession->quitNetwork(network);
    }
}

/**
 * Handle close chat request from the client.
 *
 * @param line           Line that contains close chat request.
 * @param *clientSession Client session that received the close chat request.
 */
void ClientSession::closeChat (QString line, ClientSession *clientSession)
{
    QRegularExpression re(IrGGu_Receive::closeChat);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString network = rem.captured("network");
        QString chat    = rem.captured("chat");

        Q_EMIT clientSession->closeChat(network, chat);
    }
}

/**
 * Handle query request from the client.
 *
 * @param line           Line that contains query request.
 * @param *clientSession Client session that received the query request.
 */
void ClientSession::query (QString line, ClientSession *clientSession)
{
    QRegularExpression re(IrGGu_Receive::query);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString network = rem.captured("network");
        QString nick    = rem.captured("nick");

        Q_EMIT clientSession->query(network, nick);
    }
}

/**
 * Format chat line that is going to be send to client.
 *
 * @param colors         Text colors.
 * @param timestamp      Timestamp when line was received in IRC session.
 * @param line           Line that needs to be formated.
 * @return Formatted chat line.
 */
QString ClientSession::formatChatLine(QMap<QString, QString> colors, QString timestamp,
                                      QString line)
{
    line  = "<line><timestamp><span style='color: <foreground>;'>" + timestamp
            + "</span></timestamp>" + line + "</line>";

    QRegExp regex("(<foreground>|<[a-z]{4,5}_-?[1-9]?[0-9]>)");
    QString replace;
    regex.setMinimal(true);

    for ( int i = -1; ( i = regex.indexIn(line, i+1) ) != -1; i += replace.length() )
    {
        replace = colors.value(regex.cap(1));
        line.replace(i, regex.cap(1).length(), replace);
    }

    regex.setPattern("(.{5}>|&nbsp;)((https?|ftp|sftp)://.*)(</span|&nbsp;)");

    for ( int i = -1; ( i = regex.indexIn(line, i+1) ) != -1; i += replace.length() )
    {
        replace = "<a href='" + regex.cap(2) + "'>" + regex.cap(2) + "</a>";
        line.replace((i + 6), regex.cap(2).length(), replace);
    }

    return line;
}

/**
 * Send insert network command to client or save it in commands list.
 *
 * @param network Network.
 */
void ClientSession::insertNetwork (QString network)
{
    QString command = IrGGu_Write::insertNetwork;

    command.replace("<network>", network);

    if ( socket && sessionStarted && !idle )
    {
        write(command);
    }
    else
    {
        commands.append(command);
    }
}

/**
 * Send insert chat command to client or save it in commands list.
 *
 * @param network Network.
 * @param chat    Chat.
 */
void ClientSession::insertChat (QString network, QString chat)
{
    QString command = IrGGu_Write::insertChat;

    command.replace("<network>", network);
    command.replace("<chat>", chat);

    if ( socket && sessionStarted && !idle )
    {
        write(command);
    }
    else
    {
        commands.append(command);
    }
}

/**
 * Send insert nick command to client or save it in commands list.
 *
 * @param network Network.
 * @param chat    Chat.
 * @param nick    Nick.
 */
void ClientSession::insertNick (QString network, QString chat, QString nick)
{
    QString command = IrGGu_Write::insertNick;

    command.replace("<network>", network);
    command.replace("<chat>", chat);
    command.replace("<nick>", nick);

    if ( socket && sessionStarted && !idle )
    {
        write(command);
    }
    else
    {
        commands.append(command);
    }
}

/**
 * Send insert nicks command to client or save it in commands list.
 *
 * @param network Network.
 * @param chat    Chat.
 * @param nicks   Nicks.
 */
void ClientSession::insertNicks (QString network, QString chat, QStringList nicksList)
{
    QString command = IrGGu_Write::insertNicks;

    command.replace("<network>", network);
    command.replace("<chat>", chat);

    QStringListIterator nicksListIt(nicksList);
    QString nicks;

    while ( nicksListIt.hasNext() )
    {
        nicks += " " + nicksListIt.next();
    }

    command.replace("<nicks>", nicks);

    if ( socket && sessionStarted && !idle )
    {
        write(command);
    }
    else
    {
        commands.append(command);
    }
}

/**
 * Send remove network command to client or save it in commands list.
 *
 * @param network Network.
 */
void ClientSession::removeNetwork (QString network)
{
    QString command = IrGGu_Write::removeNetwork;

    command.replace("<network>", network);

    if ( socket && sessionStarted && !idle )
    {
        write(command);
    }
    else
    {
        commands.append(command);
        allChatChanges.remove(network);
    }
}

/**
 * Send remove chat command to client or save it in commands list.
 *
 * @param network Network.
 * @param chat    Chat.
 */
void ClientSession::removeChat (QString network, QString chat)
{
    QString command = IrGGu_Write::removeChat;

    command.replace("<network>", network);
    command.replace("<chat>", chat);

    if ( socket && sessionStarted && !idle )
    {
        write(command);
    }
    else
    {
        commands.append(command);
        QMap<QString, ChatChanges *> chatChanges = allChatChanges.value(network);
        chatChanges.remove(chat);
        allChatChanges.insert(network, chatChanges);
    }
}

/**
 * Send remove nick command to client or save it in commands list.
 *
 * @param network Network.
 * @param chat    Chat.
 * @param nick    Nick.
 */
void ClientSession::removeNick (QString network, QString chat, QString nick)
{
    QString command = IrGGu_Write::removeNick;

    command.replace("<network>", network);
    command.replace("<chat>", chat);
    command.replace("<nick>", nick);

    if ( socket && sessionStarted && !idle )
    {
        write(command);
    }
    else
    {
        commands.append(command);
    }
}

/**
 * Send change nick command to client or save it in commands list.
 *
 * @param network Network.
 * @param chat    Chat.
 * @param oldNick Old nick.
 * @param newNick New nick.
 */
void ClientSession::changeNick (QString network, QString chat, QString oldNick, QString newNick)
{
    QString command = IrGGu_Write::changeNick;

    command.replace("<network>", network);
    command.replace("<chat>", chat);
    command.replace("<oldNick>", oldNick);
    command.replace("<newNick>", newNick);

    if ( socket && sessionStarted && !idle )
    {
        write(command);
    }
    else
    {
        commands.append(command);
    }
}

/**
 * Send change nick mode command to client or save it in commands list.
 *
 * @param network Network.
 * @param chat    Chat.
 * @param nick    nick.
 * @param mode    New nick mode.
 */
void ClientSession::changeNickMode (QString network, QString chat, QString nick, QString mode)
{
    QString command = IrGGu_Write::changeNickMode;

    command.replace("<network>", network);
    command.replace("<chat>", chat);
    command.replace("<nick>", nick);
    command.replace("<mode>", mode);

    command = command.trimmed();

    if ( socket && sessionStarted && !idle )
    {
        write(command);
    }
    else
    {
        commands.append(command);
    }
}

/**
 * Send rename chat command to client or save it in commands list.
 *
 * @param network     Network.
 * @param oldChatName Old chat name.
 * @param newChatName New chat name.
 */
void ClientSession::renameChat (QString network, QString oldChat, QString newChat)
{
    QString command = IrGGu_Write::renameChat;

    command.replace("<network>", network);
    command.replace("<oldChat>", oldChat);
    command.replace("<newChat>", newChat);

    if ( socket && sessionStarted && !idle )
    {
        write(command);
    }
    else
    {
        ChatChanges *chatChanges;

        if ( allChatChanges.find(network) != allChatChanges.end() )
        {
            QMap<QString, ChatChanges *> chatChangesList = allChatChanges.value(network);
            chatChanges = chatChangesList.value(oldChat, NULL);

            if ( chatChanges )
            {
                chatChangesList.remove(oldChat);
                chatChangesList.insert(newChat, chatChanges);
                allChatChanges.insert(network, chatChangesList);
            }
        }

        commands.append(command);
    }
}

/**
 * Send new message to client or save it in chat changes list.
 *
 * @param network Network.
 * @param chat    Chat.
 * @param line    Message from IRC.
 */
void ClientSession::newMsg (QString network, QString chat, QString line)
{
    if ( socket && sessionStarted && !idle )
    {
        QMap<QString, QString> timestampSettings = this->settings.value("timestamp");
        QString msg;
        QString timestamp;

        if ( timestampSettings.value("state") == "enabled" )
        {
            QString dateTimeFormat = timestampSettings.value("format");
            timestamp              = QDateTime::currentDateTime().toString(dateTimeFormat);
        }

        msg = formatChatLine(settings.value("colors"), timestamp, line);

        QString command = IrGGu_Write::newMsg;

        command.replace("<network>", network);
        command.replace("<chat>", chat);
        command.replace("<msg>", msg);

        write(command);
    }
    else if ( network != "(current)" && chat != "(current)" )
    {
        ChatChanges *chatChanges;

        if ( allChatChanges.find(network) != allChatChanges.end() )
        {
            chatChanges = allChatChanges.value(network).value(chat, NULL);

            if ( !chatChanges )
            {
                chatChanges = new ChatChanges;
                QMap<QString, ChatChanges *> chatList = allChatChanges.value(network);
                chatList.insert(chat, chatChanges);
                allChatChanges.insert(network, chatList);
            }
        }
        else
        {
            chatChanges = new ChatChanges;
            QMap<QString, ChatChanges *> chatList;
            chatList.insert(chat, chatChanges);
            allChatChanges.insert(network, chatList);
        }

        chatChanges->append.insert(Misc::getEpochNs(), line);

        if ( socket && sessionStarted && alertsEnabled && chat.at(0) != '&' && chat.at(0) != '#'
             && chat.at(0) != '+' && chat.at(0) != '!' && chat.at(0) != '('
             && clientPriority.isHighPriority(this) )
        {
            QString command = IrGGu_Write::alert;

            command.replace("<network>", network);
            command.replace("<sender>", chat);
            command.replace("<type>", QString::number(IrGGu_Alerts::newPrivateMsg));

            write(command);
        }
    }
}

/**
 * Send new highlight message to client or save it in chat changes list.
 *
 * @param network Network.
 * @param chat    Chat.
 * @param line    Highlight message from IRC.
 */
void ClientSession::newHighlightMsg (QString network, QString chat, QString line)
{
    if ( socket && sessionStarted && !idle )
    {
        QMap<QString, QString> timestampSettings = this->settings.value("timestamp");
        QString msg;
        QString timestamp;

        if ( timestampSettings.value("state") == "enabled" )
        {
            QString dateTimeFormat = timestampSettings.value("format");
            timestamp              = QDateTime::currentDateTime().toString(dateTimeFormat);
        }

        msg = formatChatLine(settings.value("colors"), timestamp, line);

        QString command = IrGGu_Write::newHighlightMsg;

        command.replace("<network>", network);
        command.replace("<chat>", chat);
        command.replace("<msg>", msg);

        write(command);
    }
    else if ( network != "(current)" && chat != "(current)" )
    {
        ChatChanges *chatChanges;

        if ( allChatChanges.find(network) != allChatChanges.end() )
        {
            chatChanges = allChatChanges.value(network).value(chat, NULL);

            if ( !chatChanges )
            {
                chatChanges = new ChatChanges;
                QMap<QString, ChatChanges *> chatList;
                chatList.insert(chat, chatChanges);
                allChatChanges.insert(network, chatList);
            }
        }
        else
        {
            chatChanges = new ChatChanges;
            QMap<QString, ChatChanges *> chatList;
            chatList.insert(chat, chatChanges);
            allChatChanges.insert(network, chatList);
        }

        chatChanges->append.insert(Misc::getEpochNs(), line);

        if ( socket && sessionStarted && alertsEnabled && clientPriority.isHighPriority(this) )
        {
            QString command = IrGGu_Write::alert;

            command.replace("<network>", network);
            command.replace("<sender>", chat);
            command.replace("<type>", QString::number(IrGGu_Alerts::newHighlightMsg));

            write(command);
        }
    }
}

/**
 * Send new own message to client or save it in chat changes list.
 *
 * @param network Network.
 * @param chat    Chat.
 * @param line    Own message from IRC.
 */
void ClientSession::newOwnMsg (QString network, QString chat, QString line)
{
    if ( socket && sessionStarted && !idle )
    {
        QMap<QString, QString> timestampSettings = this->settings.value("timestamp");
        QString msg;
        QString timestamp;

        if ( timestampSettings.value("state") == "enabled" )
        {
            QString dateTimeFormat = timestampSettings.value("format");
            timestamp              = QDateTime::currentDateTime().toString(dateTimeFormat);
        }

        msg = formatChatLine(settings.value("colors"), timestamp, line);

        QString command = IrGGu_Write::newOwnMsg;

        command.replace("<network>", network);
        command.replace("<chat>", chat);
        command.replace("<msg>", msg);

        write(command);
    }
    else if ( network != "(current)" && chat != "(current)" )
    {
        ChatChanges *chatChanges;

        if ( allChatChanges.find(network) != allChatChanges.end())
        {
            chatChanges = allChatChanges.value(network).value(chat, NULL);

            if ( !chatChanges )
            {
                chatChanges = new ChatChanges;
                QMap<QString, ChatChanges *> chatList;
                chatList.insert(chat, chatChanges);
                allChatChanges.insert(network, chatList);
            }
        }
        else
        {
            chatChanges = new ChatChanges;
            QMap<QString, ChatChanges *> chatList;
            chatList.insert(chat, chatChanges);
            allChatChanges.insert(network, chatList);
        }

        chatChanges->append.insert(Misc::getEpochNs(), line);
    }
}

/**
 * Send new DCC file command or save it in DCC files list.
 *
 * @param network Network.
 * @param sender  Sender.
 * @param file    file.
 */
void ClientSession::newDccFile (QString network, QString sender, QString file)
{
    QString command = IrGGu_Write::newDccFile;

    command.replace("<network>", network);
    command.replace("<sender>", sender);
    command.replace("<file>", file);

    if ( socket && sessionStarted && !idle )
    {
        write(command);
    }
    else
    {
        dccFiles.insert(command, alertsEnabled);
    }

    if ( socket && sessionStarted && idle && alertsEnabled && clientPriority.isHighPriority(this) )
    {
        QString command = IrGGu_Write::alert;

        command.replace("<network>", network);
        command.replace("<sender>", sender);
        command.replace("<type>", QString::number(IrGGu_Alerts::newDccFile));

        write(command);
    }
}

/**
 * Send close DCC file command or remove the file from DCC files list.
 *
 * @param network Network.
 * @param sender  Sender.
 * @param file    file.
 */
void ClientSession::closeDccFile (QString network, QString sender, QString file)
{
    QString command = IrGGu_Write::closeDccFile;

    command.replace("<network>", network);
    command.replace("<sender>", sender);
    command.replace("<file>", file);

    QString remove = IrGGu_Write::newDccFile;

    command.replace("<network>", network);
    command.replace("<sender>", sender);
    command.replace("<file>", file);

    bool alertsEnabled = false;

    if ( socket && sessionStarted && !idle )
    {
        write(command);
    }
    else if ( dccFiles.contains(remove) )
    {
        alertsEnabled = dccFiles.take(remove);
    }

    if ( socket && sessionStarted && idle && alertsEnabled )
    {
        QString command = IrGGu_Write::closeAlert;

        command.replace("<network>", network);
        command.replace("<sender>", sender);
        command.replace("<type>", QString::number(IrGGu_Alerts::newDccFile));

        write(command);
    }
}

/**
 * Remove lines from chat changes list.
 *
 * @param network Network.
 * @param chat    Chat.
 * @param lines   Count of lines to remove.
 */
void ClientSession::removeLines (QString network, QString chat)
{
    if ( !socket || !sessionStarted || idle )
    {
        ChatChanges *chatChanges;

        if ( allChatChanges.find(network) != allChatChanges.end() )
        {
            chatChanges = allChatChanges.value(network).value(chat, NULL);

            if ( !chatChanges )
            {
                chatChanges = new ChatChanges;
                QMap<QString, ChatChanges *> chatList;
                chatList.insert(chat, chatChanges);
                allChatChanges.insert(network, chatList);
            }
        }
        else
        {
            chatChanges = new ChatChanges;
            QMap<QString, ChatChanges *> chatList;
            chatList.insert(chat, chatChanges);
            allChatChanges.insert(network, chatList);
        }

        int remove = chatChanges->append.count() - *scrollback;

        for ( ; remove > 0; remove-- )
        {
            chatChanges->append.remove(chatChanges->append.keys().first());
        }
    }
}

/**
 * Read data sent by client.
 */
void ClientSession::readData ()
{
    QString line;
    QString command;

    while ( socket != NULL && socket->canReadLine() )
    {
        line = QString::fromUtf8(socket->readLine().trimmed());

        QRegularExpression commandRe(IrGGu_Commands::regex);
        QRegularExpressionMatch commandRem = commandRe.match(line);

        command = commandRem.captured("command");

        ClientSessionHandleMethod *method = NULL;

        if ( !sessionStarted && command != IrGGu_Commands::startSession
             && command != IrGGu_Commands::startTemporarySession
             && command != IrGGu_Commands::newSettingValue
             && command != IrGGu_Commands::setColors )
        {
            QString log = socket->clientAddress().toString() + " send command " + command
                          + ", before starting session! Closing connection!";

            Misc::log(log);
            socket->disconnectFromClient();
        }
        else if ( !sessionStarted && command == IrGGu_Commands::startSession )
        {
            temporary = false;

            startSession();
        }
        else if ( !sessionStarted && command == IrGGu_Commands::startTemporarySession )
        {
            temporary = true;

            startSession();
        }
        else if ( command == IrGGu_Commands::stopSession )
        {
            sessionStopped = true;
        }
        else
        {
            method = handleMethods.value(command, NULL);

            if ( method )
            {
                method(line, this);
            }
        }
    }
}

/**
 * Called when socket is disconnected.
 */
void ClientSession::disconnected()
{
    socket->deleteLater();

    socket           = NULL;
    idle             = false;
    lastDisconnected = Misc::getEpochNs();

    if ( temporary && sessionStopped )
    {
        Manager::exemptSessionId(username, sessionID);
    }
}

/**
 * Called when data has been sent to client.
 *
 * @param bytes Number of bytes written.
 */
void ClientSession::bytesWritten (qint64 bytes)
{
    if ( !buffer.isEmpty() )
    {
        BufferedLine *bufferedLine = buffer.takeFirst();

        if ( bufferedLine->bytes == bytes )
        {
            delete bufferedLine;
        }
    }
}

/**
 * This signal is emitted when client has line to write.
 *
 * @fn ClientSession::writeLine(QString network, QString chat, QString line)
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Line to be written.
 */

/**
 * This signal is emitted when client has line to complete.
 *
 * @fn ClientSession::completeLine(QString network, QString chat, QString line, int pos)
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Line to be completed.
 * @param pos     Cursor position.
 */

/**
 * This signal is emitted when client has written DCC file reply.
 *
 * @fn ClientSession::dccFileReply(QString network, QString sender, QString file, bool get)
 * @param network Network name.
 * @param sender  Sender's name.
 * @param file    Filename.
 * @param get     Specifies if the file should be accepted.
 */

/**
 * This signal is emitted when client has written close network command.
 *
 * @fn ClientSession::quitNetwork(QString network)
 * @param network Network name.
 */

/**
 * This signal is emitted when client has written close chat command.
 *
 * @fn ClientSession::closeChat(QString network, QString chat)
 * @param network Network name.
 * @param chat    Chat name.
 */

/**
 * This signal is emitted when client has written query command.
 *
 * @fn ClientSession::query(QString network, QString nick)
 * @param network Network name.
 * @param nick    Nick.
 */
