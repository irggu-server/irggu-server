/****************************************************************************
**  irgguserver.cpp
**
**  Copyright information
**
**      Copyright (C) 2011-2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "irgguserver.h"
#include "loginsession.h"
#include "clientsession.h"
#include "misc.h"
#include "config.h"
#include "manager.h"

IrGGuServer    *IrGGuServer::self;
ircSessionList IrGGuServer::ircSessions;
LocalServer    *IrGGuServer::localServer;
TcpServer      *IrGGuServer::secureTcpServer;
TcpServer      *IrGGuServer::insecureTcpServer;

/**
 * Constructor.
 */
IrGGuServer::IrGGuServer (QObject *parent) : QObject(parent)
{
}

/**
 * Start server.
 *
 * @param *parent Pointer to parent.
 */
bool IrGGuServer::start (QObject *parent)
{
    self = new IrGGuServer(parent);

    Config::set();

    bool ok          = true;
    int securePort   = Config::getConfig()->securePort;
    int insecurePort = Config::getConfig()->insecurePort;
    QString sockFile = SOCK_FILE;
    localServer      = new LocalServer(self);

    if ( localServer->listen(sockFile) )
    {
        Misc::log("Listening " + sockFile + ".");

        connect(localServer, SIGNAL(newLocalConnection(LocalSocket*)), self,
                SLOT(newLocalConnection(LocalSocket*)));
    }
    else
    {
        Misc::log("Failed to listen " + sockFile + "!");
        ok = false;
    }

    if ( securePort )
    {
        secureTcpServer = new TcpServer(true, self);

        if ( secureTcpServer->listen(QHostAddress::Any, securePort) )
        {
            Misc::log("Listening port " + QString::number(securePort)
                      + " for secure connection.");
            connect(secureTcpServer, SIGNAL(newTcpConnection(TcpSocket*)), self,
                    SLOT(newTcpConnection(TcpSocket *)));
        }
        else
        {
            Misc::log("Failed to listen port " + QString::number(securePort)
                      + " for secure connection!");
            ok = false;
        }
    }

    if ( insecurePort )
    {
        insecureTcpServer = new TcpServer(false, self);

        if ( insecureTcpServer->listen(QHostAddress::Any, insecurePort) )
        {
            Misc::log("Listening port " + QString::number(insecurePort)
                      + " for insecure connection.");
            connect(insecureTcpServer, SIGNAL(newTcpConnection(TcpSocket*)), self,
                    SLOT(newTcpConnection(TcpSocket *)));
        }
        else
        {
            Misc::log("Failed to listen port " + QString::number(insecurePort)
                      + " for insecure connection!");
            ok = false;
        }
    }

    Manager::set();
    IrcSession::set();
    ClientSession::set();

    return ok;
}

/**
 * Stop server.
 */
void IrGGuServer::stop ()
{
    if ( localServer->isListening() )
    {
        localServer->close();
    }

    if ( secureTcpServer && secureTcpServer->isListening() )
    {
        secureTcpServer->close();
    }

    if ( insecureTcpServer && insecureTcpServer->isListening() )
    {
        insecureTcpServer->close();
    }

    ircSessions.clear();
    Manager::deleteAllAccounts();
}

/**
 * Reload server configuration.
 */
void IrGGuServer::reloadConfig ()
{
    Config::set();
    Manager::set();

    Q_EMIT self->settingsChanged();
}

/**
 * Called when new IRC-client has connected to server.
 */
void IrGGuServer::newLocalConnection (LocalSocket *socket)
{
    IrcSession *ircSession = new IrcSession(socket, this);

    connect(this, SIGNAL(settingsChanged()), ircSession, SLOT(settingsChanged()));
    ircSessions.append(ircSession);
}

/**
 * Called when new client has connected to server.
 */
void IrGGuServer::newTcpConnection (TcpSocket *socket)
{
    new LoginSession(socket, this);
}

/**
 * Signal is emitted when reload of configuration is requested.
 *
 * @fn void IrGGuServer::settingsChanged()
 */
