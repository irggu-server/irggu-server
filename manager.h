/****************************************************************************
**  manager.h
**
**  Copyright information
**
**      Copyright (C) 2011-2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef MANAGER_H
#define MANAGER_H

#include "ircsession.h"
#include "clientsession.h"
#include <QtCore/QObject>
#include <QtCore/QMap>
#include <QtCore/QList>
#include <QtNetwork/QSslSocket>

struct AccountData
{
    int                       uid;
    QString                   password;
    IrcSession                *ircSession;
    QMap<int, ClientSession*> clientSessions;
    QList<int>                exemptedSessionIds;
};

/**
 *  This class is for handling registering accounts and login into clientsessions.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2014-11-16
 */
class Manager
{
public:
    static void set();
    static int  registerAccount(int uid, QString username, QString password,
                                IrcSession *ircSession, bool force = false);
    static int  login(QString username, QString password, TcpSocket *socket, int sessionID,
                      int connectionID);
    static void deleteAccount(int uid);
    static void deleteAllAccounts();
    static void exemptSessionId(QString username, int id);

private:
    static QMap<int, AccountData*>      users;
    static QMap<QString,  AccountData*> accounts;
    static int                          maxClientSessions;

};

#endif // MANAGER_H
