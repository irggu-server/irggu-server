/****************************************************************************
**  clientsession.h
**
**  Copyright information
**
**      Copyright (C) 2011-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef CLIENTSESSION_H
#define CLIENTSESSION_H

#include "network/tcpsocket.h"
#include "clientpriority.h"
#include <QtCore/QObject>
#include <QtCore/QStringList>

struct ChatChanges
{
    QMap<qint64, QString> append;
};

struct Chat
{
    QMap<qint64, QString> chat;
    QStringList           nickList;
};

struct Network
{
    QMap<QString, Chat *> chats;
};

struct BufferedLine
{
    qint64  bytes;
    QString line;
    bool    write;
};

typedef QMap<QString, Network *> NetworkList;
typedef void ClientSessionHandleMethod(QString, ClientSession *);
typedef QMap<QString, ClientSessionHandleMethod *> ClientSessionHandleMethodList;
typedef QMap<QString, QMap<QString, ChatChanges *> > AllChatChangesList;
typedef QMap<QString, QMap<QString, QString> > Settings;
typedef QList<BufferedLine *> BufferList;

/**
 *  This class is for handling client session.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2015-02-01
 */
class ClientSession : public QObject
{
    Q_OBJECT
public:
    ClientSession(QString username, int sessionID, QObject *parent = 0);
    ~ClientSession();

    void   setSocket(TcpSocket *socket);
    void   setScrollback(int *scrollback);
    void   setColors(QMap<QString, QString> colors);
    void   setTimestamp(QMap<QString, QString> timestamp);
    void   insertIrcSessionData(NetworkList networks);
    void   completedLine(QString network, QString chat, QString line);
    qint64 getLastDisconnected();
    bool   isSameClient(int connectionID);

    static void set();

private:
    TcpSocket           *socket;
    QString             username;
    QStringList         commands;
    QMap<QString, bool> dccFiles;
    AllChatChangesList  allChatChanges;
    Settings            settings;
    BufferList          buffer;
    qint64              lastDisconnected;
    int                 priority;
    int                 sessionID;
    int                 connectionID;
    int                 *scrollback;
    bool                temporary;
    bool                alertsEnabled;
    bool                sessionStarted;
    bool                sessionStopped;
    bool                idle;

    void write(QString line, bool writeBuffer = true, bool last = true);
    void writeBuffer();
    void startSession();

    static ClientPriority                clientPriority;
    static ClientSessionHandleMethodList handleMethods;

    static void    newLine(QString line, ClientSession *clientSession);
    static void    newCompleteLine(QString line, ClientSession *clientSession);
    static void    newdccFileReply(QString line, ClientSession *clientSession);
    static void    newSettingValue(QString line, ClientSession *clientSession);
    static void    setColors(QString line, ClientSession *clientSession);
    static void    setIdle(QString line, ClientSession *clientSession);
    static void    quitNetwork(QString line, ClientSession *clientSession);
    static void    closeChat(QString line, ClientSession *clientSession);
    static void    query(QString line, ClientSession *clientSession);
    static QString formatChatLine(QMap<QString, QString> colors, QString timestamp, QString line);

Q_SIGNALS:
    void writeLine(QString network, QString chat, QString line);
    void completeLine(QString network, QString chat, QString line, int pos);
    void dccFileReply(QString network, QString sender, QString file, bool get);
    void quitNetwork(QString network);
    void closeChat(QString network, QString chat);
    void query(QString network, QString nick);

public Q_SLOTS:
    void insertNetwork(QString network);
    void insertChat(QString network, QString chat);
    void insertNick(QString network, QString chat, QString nick);
    void insertNicks(QString network, QString chat, QStringList nicksList);
    void removeNetwork(QString network);
    void removeChat(QString network, QString chat);
    void removeNick(QString network, QString chat, QString nick);
    void changeNick(QString network, QString chat, QString oldNick, QString newNick);
    void changeNickMode(QString network, QString chat, QString nick, QString mode);
    void renameChat(QString network, QString oldChat, QString newChat);
    void newMsg(QString network, QString chat, QString line);
    void newHighlightMsg(QString network, QString chat, QString line);
    void newOwnMsg(QString network, QString chat, QString line);
    void newDccFile(QString network, QString sender, QString file);
    void closeDccFile(QString network, QString sender, QString file);
    void removeLines(QString network, QString chat);

private Q_SLOTS:
    void readData();
    void disconnected();
    void bytesWritten(qint64 bytes);
};

#endif // CLIENTSESSION_H
