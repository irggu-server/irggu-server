/****************************************************************************
**  config.cpp
**
**  Copyright information
**
**      Copyright (C) 2011-2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "config.h"
#include <QtCore/QSettings>

ConfigStruct Config::config;

/**
 * Set.
 */
void Config::set ()
{
    readConfig();
}

/**
 * Returns config.
 *
 * @return Config.
 */
ConfigStruct *Config::getConfig ()
{
    return &config;
}

/**
 * Read config.
 */
void Config::readConfig ()
{
    QSettings *configFile = new QSettings(CONF_FILE, QSettings::NativeFormat);

    config.securePort        = configFile->value("SecureConnection", 0).toInt();
    config.insecurePort      = configFile->value("InsecureConnection", 0).toInt();
    config.scrollback        = configFile->value("MaxScrollbackLines", 500).toInt();
    config.maxClientSessions = configFile->value("MaxClientSessions", 5).toInt();
    config.privateKey        = configFile->value("PrivateKey", "").toString();
    config.certificate       = configFile->value("Certificate", "").toString();

    delete configFile;
}
