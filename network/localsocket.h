/****************************************************************************
**  localsocket.h
**
**  Copyright information
**
**      Copyright (C) 2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef LOCALSOCKET_H
#define LOCALSOCKET_H

#include <QtNetwork/QLocalSocket>

/**
 *  This class is for IPC-socket.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2013-08-28
 */
class LocalSocket : public QObject
{
    Q_OBJECT
public:
    explicit LocalSocket(QObject *parent = 0);
    
    void       disconnectFromClient();
    int        getUid();
    bool       isUidOk();
    bool       setSocketDescriptor(qintptr socketDescriptor);
    bool       canReadLine();
    QByteArray readLine();
    qint64     write(const char *data);
    qint64     bytesToWrite();
    
private:
    QLocalSocket *socket;
    int          uid;
    bool         uidOk;
    bool         versionOk;
    bool         receivedHandshake;
    bool         sentHandshake;

    void setUid();
    void handleHandshake(QString line);
    void writeServerHandshake();

private Q_SLOTS:
    void readData();
    void wroteBytes();

Q_SIGNALS:
    void connected();
    void disconnected();
    void readyRead();
    void bytesWritten();
};

#endif // LOCALSOCKET_H
