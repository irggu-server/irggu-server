/****************************************************************************
**  localserver.cpp
**
**  Copyright information
**
**      Copyright (C) 2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "localserver.h"

/**
 * Constructor.
 *
 * @param *parent Pointer to parent.
 */
LocalServer::LocalServer (QObject *parent) : QLocalServer(parent)
{
}

/**
 * Handles incoming connections.
 *
 * @param socketDescriptor Socket descriptor.
 */
void LocalServer::incomingConnection (quintptr socketDescriptor)
{
    LocalSocket *socket = new LocalSocket(this);

    Q_EMIT newLocalConnection(socket);

    socket->setSocketDescriptor(socketDescriptor);
}

/**
 * Signal is emitted when there is a new local connection.
 *
 * @fn void LocalServer::newLocalConnection(LocalSocket *socket)
 * @param socket Local socket.
 */
