/****************************************************************************
**  tcpserver.cpp
**
**  Copyright information
**
**      Copyright (C) 2011-2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "tcpserver.h"
#include "misc.h"
#include "config.h"

/**
 * Constructor.
 *
 * @param ssl     If true use SSL.
 * @param *parent Pointer to parent.
 */
TcpServer::TcpServer (bool ssl, QObject *parent) : QTcpServer(parent)
{
    this->ssl = ssl;
}

/**
 * Handles incoming connections.
 *
 * @param socketDescriptor Socket descriptor.
 */
void TcpServer::incomingConnection (qintptr socketDescriptor)
{
    TcpSocket *socket = new TcpSocket(this);

    Q_EMIT newTcpConnection(socket);

    socket->setSocketDescriptor(socketDescriptor);

    if ( ssl )
    {
        socket->setPrivateKey(Config::getConfig()->privateKey);
        socket->setCertificate(Config::getConfig()->certificate);
        socket->startEncryption();
    }
}

/**
 * Signal is emitted when there is a new TCP connection.
 *
 * @fn void TcpServer::newTcpConnection(TcpSocket *socket)
 * @param socket TCP socket.
 */
