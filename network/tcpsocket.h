/****************************************************************************
**  tcpsocket.h
**
**  Copyright information
**
**      Copyright (C) 2013-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include "protocol/irggu.h"
#include "protocol/websocket.h"
#include <QSslSocket>
#include <QHostAddress>

enum class Handshake
{
    None,
    IrGGu,
    GetRequest,
    Upgrade,
    Connection,
    Key,
    Version
};

enum class FrameState
{
    Header,
    Length,
    Mask,
    Data
};

enum class Opcode
{
    Cont   = 0x0,
    Text   = 0x1,
    Binary = 0x2,
    R3     = 0x3,
    R4     = 0x4,
    R5     = 0x5,
    R6     = 0x6,
    R7     = 0x7,
    Close  = 0x8,
    Ping   = 0x9,
    Pong   = 0xA,
    RB     = 0xB,
    RC     = 0xC,
    RD     = 0xD,
    RE     = 0xE,
    RF     = 0xF
};

enum class CloseCode
{
    Normal                = 0x3E8,
    GoingAway             = 0x3E9,
    ProtocolError         = 0x3EA,
    DataNotSupported      = 0x3EB,
    R1004                 = 0x3EC,
    R1005                 = 0x3ED,
    R1006                 = 0x3EE,
    WrongDataType         = 0x3EF,
    PolicyViolated        = 0x3F0,
    MessageTooBig         = 0x3F1,
    UnnegotiatedExtension = 0x3F2,
    UnexpectedCondition   = 0x3F3,
    R1015                 = 0x3F7

};

struct ClientHandshake
{
    bool irggu      = false;
    bool getRequest = false;
    bool upgrade    = false;
    bool connection = false;
    bool key        = false;
    bool version    = false;
};

struct ServerHandshake
{
    QString irggu      = IrGGu_Handshake::write;
    QString status     = WebSocket_Write::status;
    QString upgrade    = WebSocket_Write::upgrade;
    QString connection = WebSocket_Write::connection;
    QString accept     = WebSocket_Write::accept;
};

struct Frame
{
    FrameState state = FrameState::Header;
    bool       final;
    Opcode     opcode;
    bool       mask;
    quint64    length;
    QByteArray maskKey;
};

struct Data
{
    QByteArray        currentData;
    QList<QByteArray> buffer;
};

typedef QMap<QString, Handshake> HandshakeList;

/**
 *  This class is for TcpSocket.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2015-02-03
 */
class TcpSocket : public QObject
{
    Q_OBJECT
public:
    explicit TcpSocket(QObject *parent = 0);

    void 	     setCertificate(const QString &file);
    void 	     setPrivateKey(const QString &file);
    void         disconnectFromClient();
    bool         setSocketDescriptor(qintptr socketDescriptor);
    bool         canReadLine();
    QByteArray   readLine();
    qint64       write(QByteArray data, bool writeACK = true);
    qint64       bytesToWrite();
    QHostAddress clientAddress();
    
private:
    QSslSocket      *socket;
    HandshakeList   handshakes;
    ClientHandshake clientHandshake;
    ServerHandshake serverHandshake;
    Frame           inFrame;
    QByteArray      outFrame;
    Data            data;
    QStringList     receivedACK;
    QList<qint64>   writtenACK;
    bool            websocket;
    bool            versionOk;
    bool            sentHandshake;
    bool            sentClose;
    bool            receivedClose;

    void handleHandshake(QString line);
    void generateAcceptHeader(QString key);
    void writeServerHandshake();
    void writeACKReply();
    void handleFrame();
    void generateFrame(Opcode opcode, const char *data);
    bool checkClose(QByteArray line);
    bool checkACK(QByteArray line);
    bool isClientHandshakeOk();

public Q_SLOTS:
    void startEncryption();

private Q_SLOTS:
    void readData();
    void wroteBytes(qint64 bytes);

Q_SIGNALS:
    void connected();
    void disconnected();
    void readyRead();
    void bytesWritten(qint64 bytes);
    
};

#endif // TCPSOCKET_H
