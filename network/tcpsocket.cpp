/****************************************************************************
**  tcpsocket.cpp
**
**  Copyright information
**
**      Copyright (C) 2013-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "tcpsocket.h"
#include <QStringListIterator>
#include <QRegularExpression>
#include <QCryptographicHash>
#include "misc.h"

/**
 * Constructor.
 *
 * @param *parent Pointer to parent.
 */
TcpSocket::TcpSocket (QObject *parent) : QObject(parent)
{
    socket        = new QSslSocket(this);
    websocket     = false;
    versionOk     = false;
    sentHandshake = false;
    sentClose     = false;
    receivedClose = false;

    connect(socket, SIGNAL(disconnected()), this, SIGNAL(disconnected()));
    connect(socket, SIGNAL(readyRead()), this, SLOT(readData()));

    handshakes.insert(IrGGu_Handshake::receive, Handshake::IrGGu);
    handshakes.insert(WebSocket_Receive::getRequest, Handshake::GetRequest);
    handshakes.insert(WebSocket_Receive::upgrade, Handshake::Upgrade);
    handshakes.insert(WebSocket_Receive::connection, Handshake::Connection);
    handshakes.insert(WebSocket_Receive::key, Handshake::Key);
    handshakes.insert(WebSocket_Receive::version, Handshake::Version);
}

/**
 * Set certificate.
 *
 * @param file Certificate file.
 */
void TcpSocket::setCertificate (const QString &file)
{
    socket->setLocalCertificate(file);
}

/**
 * Set private key.
 *
 * @param file Private key file.
 */
void TcpSocket::setPrivateKey (const QString &file)
{
    socket->setPrivateKey(file);
}

/**
 * Disconnect socket from client.
 */
void TcpSocket::disconnectFromClient ()
{
    if ( websocket )
    {
        QByteArray status;

        status.append(static_cast<quint16>(CloseCode::Normal) >> 0x8);
        status.append(static_cast<quint8>(CloseCode::Normal));

        generateFrame(Opcode::Close, status.data());

        socket->write(outFrame);

        outFrame.clear();

        sentClose = true;
    }
    else
    {
        socket->disconnectFromHost();
    }
}

/**
 * Set socket descriptor.
 *
 * @param socketDescriptor Socket descriptor.
 * @return True if socket descriptor is accepted.
 */
bool TcpSocket::setSocketDescriptor (qintptr socketDescriptor)
{
    socket->setSocketDescriptor(socketDescriptor);
}

/**
 * Return true if socket has a line to read.
 *
 * @return True if socket has a line to read.
 */
bool TcpSocket::canReadLine ()
{
    return !data.buffer.isEmpty();
}

/**
 * Read line from socket.
 *
 * @return Line from socket.
 */
QByteArray TcpSocket::readLine ()
{
    QByteArray line;

    if ( !data.buffer.isEmpty() )
    {
        line = data.buffer.takeFirst();
    }

    return line;
}

/**
 * Write data to client.
 *
 * @param data     Data to be written.
 * @param writeACK Specifies if ACK should be written.
 * @return Number of bytes that were written.
 */
qint64 TcpSocket::write (QByteArray data, bool writeACK)
{
    qint64  bytes = 0;
    QString ack   = IrGGu_Acknowledgment_Server::write + "\n";

    if ( websocket )
    {
        generateFrame(Opcode::Text, data);

        bytes = outFrame.size();

        QByteArray messageFrame = outFrame;

        outFrame.clear();

        if ( writeACK )
        {
            ack.replace("<value>", QString::number(bytes));

            generateFrame(Opcode::Text, ack.toUtf8());

            writtenACK.append(bytes);

            socket->write(outFrame);

            outFrame.clear();
        }

        socket->write(messageFrame);
    }
    else
    {
        bytes = data.size();

        if ( writeACK )
        {
            ack.replace("<value>", QString::number(bytes));

            writtenACK.append(bytes);

            socket->write(ack.toUtf8());
        }

        socket->write(data);
    }

    return bytes;
}

/**
 * Return amount of bytes that has not been written.
 *
 * @return Amount of bytes that has not been written.
 */
qint64 TcpSocket::bytesToWrite ()
{
    qint64 bytes = 0;

    QListIterator<qint64> writtenACKIt(writtenACK);

    while ( writtenACKIt.hasNext() )
    {
        bytes += writtenACKIt.next();
    }

    return bytes;
}

/**
 * Return client address.
 *
 * @return Client address.
 */
QHostAddress TcpSocket::clientAddress ()
{
    return socket->peerAddress();
}

/**
 * Handle handshake.
 *
 * @param line Handshake.
 */
void TcpSocket::handleHandshake (QString line)
{
    QStringList             keys = handshakes.keys();
    QStringListIterator     keysIt(keys);
    QRegularExpression      re;
    QRegularExpressionMatch rem;
    QString                 key;
    Handshake               handshake = Handshake::None;
    bool                    found = false;

    while ( keysIt.hasNext() && !found )
    {
        key = keysIt.next();

        re.setPattern(key);
        rem = re.match(line);

        if ( rem.hasMatch() )
        {
            found = true;

            handshake = handshakes.value(key);
        }
    }

    switch ( handshake )
    {
        case Handshake::IrGGu:
        {
            clientHandshake.irggu = true;

            versionOk = rem.captured("version").toFloat() == IrGGu::version;

            break;
        }

        case Handshake::GetRequest:
        {
            clientHandshake.getRequest = true;

            websocket = true;

            break;
        }

        case Handshake::Upgrade:
        {
            clientHandshake.upgrade = true;

            break;
        }

        case Handshake::Connection:
        {
            clientHandshake.connection = true;

            break;
        }

        case Handshake::Key:
        {
            clientHandshake.key = true;

            generateAcceptHeader(rem.captured("key"));

            break;
        }

        case Handshake::Version:
        {
            clientHandshake.version = true;

            break;
        }
    }

    if ( !socket->canReadLine() && isClientHandshakeOk() )
    {
        writeServerHandshake();
    }
}

/**
 * Generate WebSocket accept header.
 *
 * @param key WebSocket key.
 */
void TcpSocket::generateAcceptHeader (QString key)
{
    key += WebSocket_Write::guid;

    QByteArray accept = QCryptographicHash::hash(key.toUtf8(), QCryptographicHash::Sha1);

    serverHandshake.accept.replace("<base64>", accept.toBase64());
}

/**
 * Write handshake.
 */
void TcpSocket::writeServerHandshake ()
{
    QString line;

    if ( clientHandshake.irggu )
    {
        connect(socket, SIGNAL(bytesWritten(qint64)), this, SLOT(wroteBytes(qint64)));

        QString version = QString::number(IrGGu::version, 10, 1);

        line = serverHandshake.irggu + "\n";

        line.replace("<version>", version);

        write(line.toUtf8(), false);
    }
    else
    {
        line = serverHandshake.status + "\r\n";
        socket->write(line.toUtf8());

        line = serverHandshake.upgrade + "\r\n";
        socket->write(line.toUtf8());

        line = serverHandshake.connection + "\r\n";
        socket->write(line.toUtf8());

        line = serverHandshake.accept + "\r\n\r\n";
        socket->write(line.toUtf8());

        clientHandshake.getRequest = false;
        clientHandshake.upgrade    = false;
        clientHandshake.connection = false;
        clientHandshake.key        = false;
        clientHandshake.version    = false;
    }
}

/**
 * Write ACK reply.
 */
void TcpSocket::writeACKReply ()
{
    if ( !receivedACK.isEmpty() )
    {
        QString ack = IrGGu_Acknowledgment_Client::write + "\n";

        ack.replace("<value>", receivedACK.takeFirst());

        write(ack.toUtf8(), false);
    }
}

/**
 * Handle WebSocket frame.
 */
void TcpSocket::handleFrame ()
{
    switch ( inFrame.state )
    {
        case FrameState::Header:
        {
            if ( socket->bytesAvailable() < 2 )
            {
                break;
            }

            QByteArray header = socket->read(2);
            inFrame.final     = ( header.at(0) & 0x80 ) != 0;
            inFrame.opcode    = static_cast<Opcode>(header.at(0) & 0x0F);
            inFrame.mask      = ( header.at(1) & 0x80 ) != 0;
            inFrame.length    = header.at(1) & 0x7F;

            if ( inFrame.opcode == Opcode::Binary )
            {
                QByteArray status;

                status.append(static_cast<quint16>(CloseCode::DataNotSupported) >> 0x8);
                status.append(static_cast<quint8>(CloseCode::DataNotSupported));

                generateFrame(Opcode::Close, status.data());

                socket->write(outFrame);

                outFrame.clear();

                sentClose = true;
            }
        }

        case FrameState::Length:
        {
            inFrame.state = FrameState::Length;

            bool cont = true;

            switch ( inFrame.length )
            {
                case 0x7E:
                {
                    if ( socket->bytesAvailable() < 2 )
                    {
                        cont = false;

                        break;
                    }

                    QByteArray extended = socket->read(2);

                    inFrame.length  = static_cast<quint16>(extended.at(0) << 0x8);
                    inFrame.length += static_cast<quint8>(extended.at(1));

                    break;
                }

                case 0x7F:
                {
                    if ( socket->bytesAvailable() < 8 )
                    {
                        cont = false;

                        break;
                    }

                    QByteArray extended = socket->read(8);

                    inFrame.length  = static_cast<quint64>(extended.at(0) << 0x38);
                    inFrame.length += static_cast<quint64>(extended.at(1) << 0x30);
                    inFrame.length += static_cast<quint64>(extended.at(2) << 0x28);
                    inFrame.length += static_cast<quint64>(extended.at(3) << 0x20);
                    inFrame.length += static_cast<quint32>(extended.at(4) << 0x18);
                    inFrame.length += static_cast<quint32>(extended.at(5) << 0x10);
                    inFrame.length += static_cast<quint16>(extended.at(6) << 0x8);
                    inFrame.length += static_cast<quint8>(extended.at(7));

                    break;
                }
            }

            if ( !cont )
            {
                break;
            }
        }

        case FrameState::Mask:
        {
            inFrame.state = FrameState::Mask;

            if ( inFrame.mask )
            {
                if ( socket->bytesAvailable() < 4 )
                {
                    break;
                }

                inFrame.maskKey = socket->read(4);
            }
        }

        case FrameState::Data:
        {
            inFrame.state = FrameState::Data;

            if ( socket->bytesAvailable() < inFrame.length )
            {
                break;
            }

            QByteArray data = socket->read(inFrame.length);

            if ( inFrame.mask )
            {
                for ( int i = 0; i < data.size(); i++ )
                {
                    data[i] = ( data.at(i) ^ inFrame.maskKey.at(i % 4) );
                }
            }

            switch ( inFrame.opcode )
            {
                case Opcode::Cont:
                case Opcode::Text:
                {
                    this->data.currentData.append(data);

                    if ( inFrame.final )
                    {
                        if ( !sentHandshake )
                        {
                            handleHandshake(QString::fromUtf8(this->data.currentData));
                        }
                        else
                        {
                            if ( !checkClose(this->data.currentData) )
                            {
                                bool ack = checkACK(this->data.currentData);

                                if ( !ack && !receivedACK.isEmpty() )
                                {
                                    this->data.buffer.append(this->data.currentData);

                                    writeACKReply();

                                    Q_EMIT readyRead();
                                }
                                else if ( !ack && receivedACK.isEmpty() )
                                {
                                    disconnectFromClient();
                                }
                            }
                            else
                            {
                                disconnectFromClient();
                            }
                        }

                        this->data.currentData.clear();
                    }

                    break;
                }

                case Opcode::Close:
                {
                    connect(socket, SIGNAL(bytesWritten(qint64)), this, SLOT(wroteBytes(qint64)));

                    receivedClose = true;

                    QByteArray status;

                    if ( data.size() >= 2 )
                    {
                        status.append(data.at(0));
                        status.append(data.at(1));
                    }

                    generateFrame(Opcode::Close, status.data());

                    socket->write(outFrame);

                    outFrame.clear();

                    break;
                }
            }

            inFrame.state = FrameState::Header;
        }
    }
}

/**
 * Generate WebSocket frame.
 *
 * @param opcode WebSocket operation code.
 * @param data   Frame data.
 */
void TcpSocket::generateFrame (Opcode opcode, const char *data)
{
    outFrame.append(static_cast<uint>(opcode) | 0x80);

    quint64 length = strlen(data);

    if ( length > 0x7D )
    {
        if ( length <= 0xFFFF )
        {
            outFrame.append(0x7E);
            outFrame.append(length >> 0x8);
            outFrame.append(length);
        }
        else if ( length <= 0x7FFFFFFFFFFFFFFF)
        {
            outFrame.append(0x7F);
            outFrame.append(length >> 0x38);
            outFrame.append(length >> 0x30);
            outFrame.append(length >> 0x28);
            outFrame.append(length >> 0x20);
            outFrame.append(length >> 0x18);
            outFrame.append(length >> 0x10);
            outFrame.append(length >> 0x8);
            outFrame.append(length);
        }
    }
    else
    {
        outFrame.append(length);
    }

    outFrame.append(data);
}

/**
 * Check if line is CLOSE message.
 *
 * @param line Line from client.
 * @return True if line is CLOSE message.
 */
bool TcpSocket::checkClose (QByteArray line)
{
    bool    close        = false;
    QString closeMessage = QString::fromUtf8(line).trimmed();

    QRegularExpression      re(IrGGu_Close::receive);
    QRegularExpressionMatch rem = re.match(closeMessage);

    if ( rem.hasMatch() )
    {
        close = true;
    }

    return close;
}


/**
 * Check if line is ACK message.
 *
 * @param line Line from client.
 * @return True if line is ACK message.
 */
bool TcpSocket::checkACK (QByteArray line)
{
    bool    ack        = false;
    QString ackMessage = QString::fromUtf8(line).trimmed();

    QRegularExpression      reS(IrGGu_Acknowledgment_Server::receive);
    QRegularExpressionMatch remS = reS.match(ackMessage);
    QRegularExpression      reC(IrGGu_Acknowledgment_Client::receive);
    QRegularExpressionMatch remC = reC.match(ackMessage);

    if ( remS.hasMatch() )
    {
        ack = true;

        qint64 bytes = writtenACK.takeFirst();

        if ( bytes == remS.captured("value").toLongLong() )
        {
            wroteBytes(bytes);
        }
        else
        {
            disconnectFromClient();
        }
    }
    else if ( remC.hasMatch() )
    {
        ack = true;

        receivedACK.append(remC.captured("value"));
    }

    return ack;
}

/**
 * Return true if client handshake is ok.
 *
 * @return True if client handshake is ok.
 */
bool TcpSocket::isClientHandshakeOk ()
{
    bool ok = false;

    if ( clientHandshake.irggu )
    {
        ok = true;
    }
    else if ( clientHandshake.getRequest && clientHandshake.upgrade && clientHandshake.connection
              && clientHandshake.key && clientHandshake.version )
    {
        ok = true;
    }

    return ok;
}

/**
 * Start encryption.
 */
void TcpSocket::startEncryption ()
{
    socket->startServerEncryption();
}

/**
 * Read data sent by client.
 */
void TcpSocket::readData ()
{
    if ( !sentHandshake && !websocket )
    {
        while ( socket->state() == QSslSocket::ConnectedState && socket->canReadLine() )
        {
            QString line = QString::fromUtf8(socket->readLine()).trimmed();

            handleHandshake(line);
        }
    }
    else if ( websocket )
    {
        if ( sentClose )
        {
            websocket = false;

            disconnectFromClient();
        }
        else
        {
            handleFrame();
        }
    }
    else
    {
        while ( socket->state() == QSslSocket::ConnectedState && socket->canReadLine() )
        {
            QByteArray data = socket->readLine();

            if ( !checkClose(data) )
            {
                bool ack  = checkACK(data);

                if ( !ack && !receivedACK.isEmpty() )
                {
                    this->data.buffer.append(data);

                    writeACKReply();

                    Q_EMIT readyRead();
                }
                else if ( !ack )
                {
                    disconnectFromClient();
                }
            }
            else
            {
                disconnectFromClient();
            }
        }
    }
}

/**
 * Called when data has been sent to client.
 *
 * @param bytes Number of bytes written.
 */
void TcpSocket::wroteBytes (qint64 bytes)
{
    if ( !sentHandshake )
    {
        if ( versionOk )
        {
            sentHandshake = true;

            Q_EMIT connected();

            disconnect(socket, SIGNAL(bytesWritten(qint64)), this, SLOT(wroteBytes(qint64)));
        }
        else if ( socket->bytesToWrite() == 0 )
        {
            disconnectFromClient();
        }
    }
    else if ( websocket && receivedClose && socket->bytesToWrite() == 0 )
    {
        websocket = false;

        disconnect(socket, SIGNAL(bytesWritten(qint64)), this, SLOT(wroteBytes(qint64)));
    }
    else
    {
        Q_EMIT bytesWritten(bytes);
    }
}

/**
 * Signal is emitted when socket is in connected state.
 *
 * @fn void TcpSocket::connected()
 */

/**
 * Signal is emitted when socket is in disconnected state.
 *
 * @fn void TcpSocket::disconnected()
 */

/**
 * Signal is emitted when socket has data for reading.
 *
 * @fn void TcpSocket::readyRead()
 */

/**
 * Signal is emitted when socket wrote data.
 *
 * @fn void TcpSocket::bytesWritten(qint64 bytes)
 * @param bytes Number of bytes written.
 */

