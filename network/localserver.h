/****************************************************************************
**  localserver.h
**
**  Copyright information
**
**      Copyright (C) 2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef LOCALSERVER_H
#define LOCALSERVER_H

#include "network/localsocket.h"
#include <QtNetwork/QLocalServer>

/**
 *  This class is for IPC-server.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2013-08-23
 */
class LocalServer : public QLocalServer
{
    Q_OBJECT
public:
    explicit LocalServer(QObject *parent = 0);

protected:
    void incomingConnection(quintptr socketDescriptor);

Q_SIGNALS:
    void newLocalConnection(LocalSocket *socket);

};

#endif // LOCALSERVER_H
