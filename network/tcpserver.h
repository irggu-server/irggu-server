/****************************************************************************
**  tcpserver.h
**
**  Copyright information
**
**      Copyright (C) 2011-2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef TCPSERVER_H
#define TCPSERVER_H

#include "network/tcpsocket.h"
#include <QTcpServer>

/**
 *  This class is for TCP-server.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2013-08-22
 */
class TcpServer : public QTcpServer
{
    Q_OBJECT
public:
    TcpServer(bool ssl, QObject *parent = 0);

protected:
    void incomingConnection(qintptr socketDescriptor);

private:
    bool ssl;

Q_SIGNALS:
    void newTcpConnection(TcpSocket *socket);

};

#endif // TCPSERVER_H
