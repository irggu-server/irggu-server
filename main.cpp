/****************************************************************************
**  main.cpp
**
**  Copyright information
**
**      Copyright (C) 2011-2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "irgguserver.h"
#include "config.h"
#include "misc.h"
#include <QtCore/QCoreApplication>
#include <fcntl.h>
#include <cstdlib>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <signal.h>

/**
 * Handles signal sent to the process.
 *
 * @param sig Signal.
 */
void signal_handler (int sig)
{
        switch ( sig )
        {
            case SIGHUP:
            {
                Misc::log("hangup signal catched.");
                IrGGuServer::reloadConfig();

                break;
            }

            case SIGTERM:
            {
                Misc::log("terminate signal catched.");
                IrGGuServer::stop();
                exit(0);

                break;
            }
        }
}

/**
 * Create daemon.
 */
void daemonize ()
{
    pid_t pid = fork();

    if ( pid < 0 )
    {
        Misc::log("Fork failed!");
        exit(1);
    }

    if ( pid > 0 )
    {
        exit(0);
    }

    umask(0);

    if ( setsid() < 0 )
    {
        Misc::log("Failed to create SID!");
    }

    for ( int d = getdtablesize(); d >= 0; --d )
    {
        close(d);
    }

    if ( chdir("/tmp") != 0 )
    {
        Misc::log("Failed to change running directory!");
        exit(1);
    }

    Misc::writePid();

    signal(SIGCHLD,SIG_IGN);
    signal(SIGTSTP,SIG_IGN);
    signal(SIGTTOU,SIG_IGN);
    signal(SIGTTIN,SIG_IGN);
    signal(SIGHUP,signal_handler);
    signal(SIGTERM,signal_handler);
}

/**
 * Main.
 *
 * @param argc  Argument count.
 * @param *argv Pointer to argument values.
 */
int main (int argc, char *argv[])
{
    Misc::set();

    daemonize();

    QCoreApplication a(argc, argv);

    Misc::log("Starting server.");

    if ( !IrGGuServer::start(&a) )
    {
        Misc::log("Failed to start server!");
        exit(1);
    }

    return a.exec();
}
