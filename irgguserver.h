/****************************************************************************
**  irgguserver.h
**
**  Copyright information
**
**      Copyright (C) 2011-2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef IRGGUSERVER_H
#define IRGGUSERVER_H

#include "ircsession.h"
#include "network/tcpserver.h"
#include "network/localserver.h"
#include <QtCore/QObject>
#include <QtCore/QList>

typedef QList<IrcSession *> ircSessionList;

/**
 *  This class is the main class that creates local and TCP-servers.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2013-08-22
 */
class IrGGuServer : public QObject
{
    Q_OBJECT
public:
    explicit IrGGuServer(QObject *parent = 0);

    static bool start(QObject *parent);
    static void stop();
    static void reloadConfig();

private:
    static IrGGuServer    *self;
    static ircSessionList ircSessions;
    static LocalServer    *localServer;
    static TcpServer      *secureTcpServer;
    static TcpServer      *insecureTcpServer;

Q_SIGNALS:
    void settingsChanged();

private Q_SLOTS:
    void newLocalConnection(LocalSocket *socket);
    void newTcpConnection(TcpSocket *socket);
};

#endif // IRGGUSERVER_H
