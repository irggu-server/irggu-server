/****************************************************************************
**  loginsession.cpp
**
**  Copyright information
**
**      Copyright (C) 2011-2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "loginsession.h"
#include "manager.h"
#include "misc.h"
#include "protocol/irggu.h"
#include <QtNetwork/QHostAddress>
#include <QRegularExpression>

/**
 * Constructor.
 *
 * @param *socket Socket.
 * @param *parent Pointer to parent.
 */
LoginSession::LoginSession (TcpSocket *socket, QObject *parent) : QObject(parent)
{
    this->socket = socket;
    this->socket->setParent(this);

    connect(socket, SIGNAL(connected()), this, SLOT(connected()));
    connect(socket, SIGNAL(bytesWritten(qint64)), this, SLOT(bytesWritten()));
    connect(socket, SIGNAL(readyRead()), this, SLOT(readData()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
}

/**
 * Send line to client.
 *
 * @param line Line to write.
 */
void LoginSession::write (QString line)
{
    line += "\n";
    socket->write(line.toUtf8());
}

/**
 * Called when client has connected to server.
 */
void LoginSession::connected ()
{
    Misc::log("Client connected from " + socket->clientAddress().toString() + ".");
}

/**
 * Called when client has disconnected.
 */
void LoginSession::disconnected ()
{
    this->deleteLater();
}

/**
 * Read data sent by client.
 */
void LoginSession::readData ()
{
    QString line;
    QString command;

    while ( socket->canReadLine() )
    {
        line = QString::fromUtf8(socket->readLine().trimmed());

        QRegularExpression commandRe("^(?<command>\\S+).*$");
        QRegularExpressionMatch commandRem = commandRe.match(line);

        command = commandRem.captured("command");

        QRegularExpression re(IrGGu_Receive::login);
        QRegularExpressionMatch rem = re.match(line);

        if ( command != "LOGIN" )
        {
            QString log = socket->clientAddress().toString() + " send command " + command
                          + ", before successfully login! Closing connection!";

            Misc::log(log);
            socket->disconnectFromClient();
        }
        else if ( rem.hasMatch() )
        {
            command = IrGGu_Write::login;

            int sessionID    = rem.captured("sessionID").toInt();
            int connectionID = rem.captured("connectionID").toInt();
            int login        = Manager::login(rem.captured("username"), rem.captured("password"),
                                              socket, sessionID, connectionID);
            if ( login != 0 )
            {
                QString log = "Failed authentication from " + socket->clientAddress().toString();

                Misc::log(log);

                command.replace("<result>", QString::number(login));

                write(command);
            }
            else
            {
                disconnect(socket, SIGNAL(bytesWritten(qint64)), this, SLOT(bytesWritten()));
                deleteLater();
            }
        }
        else
        {
            command = IrGGu_Write::login;

            command.replace("<result>", QString::number(IrGGu_Login::wrongUserPass));

            write(command);
        }
    }
}

/**
 * Called when data has been sent to client.
 */
void LoginSession::bytesWritten ()
{
    if ( socket->bytesToWrite() == 0 && socket->parent() == this )
    {
        socket->disconnectFromClient();
    }
}
