/****************************************************************************
**  websocket.h
**
**  Copyright information
**
**      Copyright (C) 2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef WEBSOCKET_H
#define WEBSOCKET_H
#include <QString>

namespace WebSocket_Receive
{
    const QString getRequest = "^GET\\s/\\S*\\sHTTP/1.1$";
    const QString upgrade    = "^Upgrade:\\swebsocket$";
    const QString connection = "^Connection:\\s.*Upgrade.*$";
    const QString key        = "^Sec-WebSocket-Key:\\s(?<key>\\S{24})$";
    const QString version    = "^Sec-WebSocket-Version:\\s13$";
};

namespace WebSocket_Write
{
    const QString status     = "HTTP/1.1 101 Switching Protocols";
    const QString upgrade    = "Upgrade: websocket";
    const QString connection = "Connection: Upgrade";
    const QString accept     = "Sec-WebSocket-Accept: <base64>";
    const QString guid       = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
}

#endif // WEBSOCKET_H
