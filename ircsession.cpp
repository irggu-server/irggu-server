/****************************************************************************
**  ircsession.cpp
**
**  Copyright information
**
**      Copyright (C) 2011-2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "protocol/irggu.h"
#include "ircsession.h"
#include "manager.h"
#include "misc.h"
#include "config.h"
#include <QtCore/QDateTime>
#include <QRegularExpression>

IrcSessionHandleMethodList IrcSession::handleMethods;

/**
 * Constructor.
 *
 * @param socket  Socket.
 * @param *parent Pointer to parent.
 */
IrcSession::IrcSession (LocalSocket *socket, QObject *parent) : QObject(parent)
{
    this->socket       = socket;
    this->isRegistered = false;
    this->scrollback   = Config::getConfig()->scrollback;
    this->socket->setParent(this);

    this->connect(socket, SIGNAL(readyRead()), this, SLOT(readData()));
    this->connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));

    QMap<QString, QString> colors;
    colors.insert("foreground", "#000000");
    colors.insert("local_0", "#CCCCCC");
    colors.insert("local_1", "#000000");
    colors.insert("local_2", "#3636B2");
    colors.insert("local_3", "#2A8C2A");
    colors.insert("local_4", "#C33B3B");
    colors.insert("local_5", "#C73232");
    colors.insert("local_6", "#80267F");
    colors.insert("local_7", "#66361F");
    colors.insert("local_8", "#4C4C4C");
    colors.insert("local_9", "#4545E6");
    colors.insert("local_10", "#1A5555");
    colors.insert("local_11", "#2F8C74");
    colors.insert("local_12", "#4545E6");
    colors.insert("local_13", "#B037B0");
    colors.insert("local_14", "#C73232");
    colors.insert("local_15", "#959595");
    colors.insert("mirc_0", "#CCCCCC");
    colors.insert("mirc_1", "#000000");
    colors.insert("mirc_2", "#3636B2");
    colors.insert("mirc_3", "#2A8C2A");
    colors.insert("mirc_4", "#C33B3B");
    colors.insert("mirc_5", "#C73232");
    colors.insert("mirc_6", "#80267F");
    colors.insert("mirc_7", "#66361F");
    colors.insert("mirc_8", "#D9A641");
    colors.insert("mirc_9", "#3DCC3D");
    colors.insert("mirc_10", "#1A5555");
    colors.insert("mirc_11", "#2F8C74");
    colors.insert("mirc_12", "#4545E6");
    colors.insert("mirc_13", "#B037B0");
    colors.insert("mirc_14", "#4C4C4C");
    colors.insert("mirc_15", "#959595");

    QMap<QString, QString> timestamp;
    timestamp.insert("state", "enabled");
    timestamp.insert("format", "[dd.MM.yy hh:mm:ss]");

    this->settings.insert("colors", colors);
    this->settings.insert("timestamp", timestamp);
}

/**
 * Write line to IRC client.
 *
 * @param line Line to write.
 */
void IrcSession::write (QString line)
{
    line += "\n";
    socket->write(line.toUtf8());
}

/**
 * Insert client session.
 *
 * @param *clientSession Pointer to client session.
 */
void IrcSession::insertClientSession (ClientSession *clientSession)
{
    clientSession->setParent(this);
    clientSession->setScrollback(&scrollback);
    clientSession->setColors(settings.value("colors"));
    clientSession->setTimestamp(settings.value("timestamp"));
    clientSession->insertIrcSessionData(networks);

    connect(this, SIGNAL(networkAdded(QString)), clientSession, SLOT(insertNetwork(QString)));
    connect(this, SIGNAL(chatAdded(QString,QString)), clientSession,
            SLOT(insertChat(QString,QString)));
    connect(this, SIGNAL(nickAdded(QString,QString,QString)), clientSession,
            SLOT(insertNick(QString,QString,QString)));
    connect(this, SIGNAL(nicksAdded(QString,QString,QStringList)), clientSession,
            SLOT(insertNicks(QString,QString,QStringList)));
    connect(this, SIGNAL(networkRemoved(QString)), clientSession, SLOT(removeNetwork(QString)));
    connect(this, SIGNAL(chatRemoved(QString,QString)), clientSession,
            SLOT(removeChat(QString,QString)));
    connect(this, SIGNAL(nickRemoved(QString,QString,QString)), clientSession,
            SLOT(removeNick(QString,QString,QString)));
    connect(this, SIGNAL(nickChanged(QString,QString,QString,QString)), clientSession,
            SLOT(changeNick(QString,QString,QString,QString)));
    connect(this, SIGNAL(nickModeChanged(QString,QString,QString,QString)), clientSession,
            SLOT(changeNickMode(QString,QString,QString,QString)));
    connect(this, SIGNAL(chatRenamed(QString,QString,QString)), clientSession,
            SLOT(renameChat(QString,QString,QString)));
    connect(this, SIGNAL(msgAdded(QString,QString,QString)), clientSession,
            SLOT(newMsg(QString,QString,QString)));
    connect(this, SIGNAL(highlightMsgAdded(QString,QString,QString)), clientSession,
            SLOT(newHighlightMsg(QString,QString,QString)));
    connect(this, SIGNAL(ownMsgAdded(QString,QString,QString)), clientSession,
            SLOT(newOwnMsg(QString,QString,QString)));
    connect(this, SIGNAL(incommingDccFile(QString,QString,QString)), clientSession,
            SLOT(newDccFile(QString,QString,QString)));
    connect(this, SIGNAL(closeDccFile(QString,QString,QString)), clientSession,
            SLOT(closeDccFile(QString,QString,QString)));
    connect(this, SIGNAL(linesRemoved(QString,QString)), clientSession,
            SLOT(removeLines(QString,QString)));
    connect(clientSession, SIGNAL(writeLine(QString,QString,QString)), this,
            SLOT(writeLine(QString,QString,QString)));
    connect(clientSession, SIGNAL(completeLine(QString,QString,QString,int)), this,
            SLOT(completeLine(QString,QString,QString,int)));
    connect(clientSession, SIGNAL(dccFileReply(QString,QString,QString,bool)), this,
            SLOT(dccFileReply(QString,QString,QString,bool)));
    connect(clientSession, SIGNAL(quitNetwork(QString)), this, SLOT(quitNetwork(QString)));
    connect(clientSession, SIGNAL(closeChat(QString,QString)), this,
            SLOT(closeChat(QString,QString)));
    connect(clientSession, SIGNAL(query(QString,QString)), this, SLOT(query(QString,QString)));
}

/**
 * Set handle methods list.
 */
void IrcSession::set ()
{
    handleMethods.insert(IrGGu_IRC_Commands::newSettingValue, &newSettingValue);
    handleMethods.insert(IrGGu_IRC_Commands::insertNetwork, &insertNetwork);
    handleMethods.insert(IrGGu_IRC_Commands::insertChat, &insertChat);
    handleMethods.insert(IrGGu_IRC_Commands::insertNick, &insertNick);
    handleMethods.insert(IrGGu_IRC_Commands::insertNicks, &insertNicks);
    handleMethods.insert(IrGGu_IRC_Commands::removeNetwork, &removeNetwork);
    handleMethods.insert(IrGGu_IRC_Commands::removeChat, &removeChat);
    handleMethods.insert(IrGGu_IRC_Commands::removeNick, &removeNick);
    handleMethods.insert(IrGGu_IRC_Commands::changeNick, &changeNick);
    handleMethods.insert(IrGGu_IRC_Commands::changeNickMode, &changeNickMode);
    handleMethods.insert(IrGGu_IRC_Commands::renameChat, &renameChat);
    handleMethods.insert(IrGGu_IRC_Commands::newMsg, &newMsg);
    handleMethods.insert(IrGGu_IRC_Commands::newHighlightMsg, &newHighlightMsg);
    handleMethods.insert(IrGGu_IRC_Commands::newOwnMsg, &newOwnMsg);
    handleMethods.insert(IrGGu_IRC_Commands::newDccFile, &newDccFile);
    handleMethods.insert(IrGGu_IRC_Commands::closeDccFile, &closeDccFile);
    handleMethods.insert(IrGGu_IRC_Commands::lineCompleted, &lineCompleted);
}

/**
 * Set count of scrollback lines.
 *
 * @param scrollback New count of scrollback lines.
 */
void IrcSession::setScrollback (int scrollback)
{
    if ( this->scrollback > scrollback )
    {
        QMapIterator<QString, Network *> networksIt(networks);
        Network *network;
        Chat *chat;
        QString networkName;
        QString chatName;
        QMap<qint64, QString> *chatText;
        int remove;

        while ( networksIt.hasNext() )
        {
            networksIt.next();
            network     = networksIt.value();
            networkName = networksIt.key();

            QMapIterator<QString, Chat *> chatsIt(network->chats);

            while ( chatsIt.hasNext() )
            {
                chatsIt.next();
                chat        = chatsIt.value();
                chatName    = chatsIt.key();
                chatText    = &chat->chat;
                remove      = chatText->count() - scrollback;

                if ( remove > 0 )
                {
                    for ( int lines = remove; lines > 0; lines-- )
                    {
                        chatText->remove(chatText->keys().first());
                    }

                    Q_EMIT linesRemoved(networkName, chatName);
                }
            }
        }
    }

    this->scrollback = scrollback;
}

/**
 * Register IRC client to server.
 *
 * @param line  Line that contains the account information from IRC client.
 * @param force Force registration.
 */
void IrcSession::registerSession (QString line, bool force)
{
    QString command = IrGGu_IRC_Write::registerIRC;

    if ( socket->isUidOk() )
    {
        uid = socket->getUid();

        QRegularExpression re(IrGGu_IRC_Receive::registerIRC);
        QRegularExpressionMatch rem = re.match(line);

        if ( rem.hasMatch() )
        {
            QString username    = rem.captured("username");
            QString password    = rem.captured("password");
            int registertration = Manager::registerAccount(uid, username, password, this, force);
            isRegistered        = registertration == 0;

            command.replace("<result>", QString::number(registertration));

            write(command);

            if ( isRegistered )
            {
                Misc::log("User " + QString::number(uid) + " registered on the server.");
            }
        }
        else
        {
            command.replace("<result>", QString::number(IrGGu_IRC_Register::userPassNotValid));

            write(command);
        }
    }
    else
    {
        connect(socket, SIGNAL(bytesWritten()), this, SLOT(bytesWritten()));

        command.replace("<result>", QString::number(IrGGu_IRC_Register::unableToGetUID));

        write(command);

        Misc::log("Unable to get UID! Closing connection!");
    }
}

/**
 * Handle setting change from the IRC client.
 *
 * @param line        Line that contains insert network command.
 * @param *networks   List of networks that contains all IRC data.
 * @param *ircSession IRC session that received the insert network command.
 */
void IrcSession::newSettingValue (QString line, NetworkList *networks, IrcSession *ircSession)
{
    QRegularExpression re(IrGGu_IRC_Receive::newSettingValue);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString category = rem.captured("category");
        QString setting  = rem.captured("setting");
        QString value    = rem.captured("value");
        QString command  = IrGGu_IRC_Write::clientNotice;

        if ( category == "scrollback" )
        {
            int scrollback    = value.toInt();
            int maxScrollback = Config::getConfig()->scrollback;
            QString msg       = "";

            if ( scrollback > maxScrollback )
            {
                ircSession->setScrollback(maxScrollback);

                msg += "<line>Maximum allowed count of scrollback lines is "
                       + QString::number(maxScrollback) + ".</line>";
            }
            else
            {
                ircSession->setScrollback(scrollback);
            }

            msg += "<line>Count of scrollback lines is now " + QString::number(ircSession->scrollback)
                   + ".</line>";

            command.replace("<msg>", msg);

            ircSession->write(command);
        }
        else
        {
            QMap<QString, QString> settingsList = ircSession->settings.value(category);
            settingsList.insert(setting, value);
            ircSession->settings.insert(category, settingsList);

            QString msg = "<line>Setting " + setting + " in category " + category
                          + " is now setted to " + value + ".</line>";

            command.replace("<msg>", msg);

            ircSession->write(command);
        }
    }
}

/**
 * Handle colors change from the IRC client.
 *
 * @param line        Line that contains insert network command.
 * @param *networks   List of networks that contains all IRC data.
 * @param *ircSession IRC session that received the insert network command.
 */
void IrcSession::setColors (QString line, NetworkList *networks, IrcSession *ircSession)
{
    QRegularExpression re(IrGGu_IRC_Receive::setColors);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QStringList colors = rem.captured("colors").split(QRegularExpression("\\s"));

        if ( colors.length() == 33 )
        {
            QString category                  = "colors";
            QMap<QString, QString> colorsList = ircSession->settings.value(category);

            colorsList.insert("foreground", colors.at(0));
            colorsList.insert("local_0", colors.at(1));
            colorsList.insert("local_1", colors.at(2));
            colorsList.insert("local_2", colors.at(3));
            colorsList.insert("local_3", colors.at(4));
            colorsList.insert("local_4", colors.at(5));
            colorsList.insert("local_5", colors.at(6));
            colorsList.insert("local_6", colors.at(7));
            colorsList.insert("local_7", colors.at(8));
            colorsList.insert("local_8", colors.at(9));
            colorsList.insert("local_9", colors.at(10));
            colorsList.insert("local_10", colors.at(11));
            colorsList.insert("local_11", colors.at(12));
            colorsList.insert("local_12", colors.at(13));
            colorsList.insert("local_13", colors.at(14));
            colorsList.insert("local_14", colors.at(15));
            colorsList.insert("local_15", colors.at(16));
            colorsList.insert("mirc_0", colors.at(17));
            colorsList.insert("mirc_1", colors.at(18));
            colorsList.insert("mirc_2", colors.at(19));
            colorsList.insert("mirc_3", colors.at(20));
            colorsList.insert("mirc_4", colors.at(21));
            colorsList.insert("mirc_5", colors.at(22));
            colorsList.insert("mirc_6", colors.at(23));
            colorsList.insert("mirc_7", colors.at(24));
            colorsList.insert("mirc_8", colors.at(25));
            colorsList.insert("mirc_9", colors.at(26));
            colorsList.insert("mirc_10",colors.at(27));
            colorsList.insert("mirc_11", colors.at(28));
            colorsList.insert("mirc_12", colors.at(29));
            colorsList.insert("mirc_13", colors.at(30));
            colorsList.insert("mirc_14", colors.at(31));
            colorsList.insert("mirc_15", colors.at(32));

            ircSession->settings.insert(category, colorsList);
        }
    }
}

/**
 * Handle insert network command from the IRC client.
 *
 * @param line        Line that contains insert network command.
 * @param *networks   List of networks that contains all IRC data.
 * @param *ircSession IRC session that received the insert network command.
 */
void IrcSession::insertNetwork (QString line, NetworkList *networks, IrcSession *ircSession)
{
    QRegularExpression re(IrGGu_IRC_Receive::insertNetwork);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        Network *network    = new Network;
        Chat *chat          = new Chat;
        QString networkName = rem.captured("network");
        QString chatName    = "(server)";
        networks->insert(networkName, network);
        network->chats.insert(chatName, chat);

        Q_EMIT ircSession->networkAdded(networkName);
    }
}

/**
 * Handle insert chat command from the IRC client.
 *
 * @param line        Line that contains insert chat command.
 * @param *networks   List of networks that contains all IRC data.
 * @param *ircSession IRC session that received the insert chat command.
 */
void IrcSession::insertChat (QString line, NetworkList *networks, IrcSession *ircSession)
{
    QRegularExpression re(IrGGu_IRC_Receive::insertChat);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString networkName = rem.captured("network");
        Network *network    = networks->value(networkName, NULL);

        if ( network )
        {
            Chat *chat       = new Chat;
            QString chatName = rem.captured("chat");
            network->chats.insert(chatName, chat);

            Q_EMIT ircSession->chatAdded(networkName, chatName);
        }
    }
}

/**
 * Handle insert nick command from the IRC client.
 *
 * @param line        Line that contains insert nick command.
 * @param *networks   List of networks that contains all IRC data.
 * @param *ircSession IRC session that received the insert nick command.
 */
void IrcSession::insertNick (QString line, NetworkList *networks, IrcSession *ircSession)
{
    QRegularExpression re(IrGGu_IRC_Receive::insertNick);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString networkName = rem.captured("network");
        Network *network    = networks->value(networkName, NULL);

        if ( network )
        {
            QString chatName = rem.captured("chat");
            Chat *chat       = network->chats.value(chatName, NULL);

            if ( chat )
            {
                QString nick = rem.captured("nick");
                chat->nickList.append(nick);

                Q_EMIT ircSession->nickAdded(networkName, chatName, nick);
            }
        }
    }
}

/**
 * Handle insert nicks command from the IRC client.
 *
 * @param line        Line that contains insert nicks command.
 * @param *networks   List of networks that contains all IRC data.
 * @param *ircSession IRC session that received the insert nicks command.
 */
void IrcSession::insertNicks (QString line, NetworkList *networks, IrcSession *ircSession)
{
    QRegularExpression re(IrGGu_IRC_Receive::insertNicks);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString networkName = rem.captured("network");
        Network *network    = networks->value(networkName, NULL);

        if ( network )
        {
            QString chatName = rem.captured("chat");
            Chat *chat       = network->chats.value(chatName, NULL);

            if ( chat )
            {
                QStringList nicks = rem.captured("nicks").split(QRegularExpression("\\s"));

                for ( int i = 0; i <= nicks.count(); i++ )
                {
                    nicks.append(nicks.at(i));
                }

                chat->nickList.append(nicks);

                Q_EMIT ircSession->nicksAdded(networkName, chatName, nicks);
            }
        }
    }
}

/**
 * Handle remove network command from the IRC client.
 *
 * @param line        Line that contains remove network command.
 * @param *networks   List of networks that contains all IRC data.
 * @param *ircSession IRC session that received the remove network command.
 */
void IrcSession::removeNetwork (QString line, NetworkList *networks, IrcSession *ircSession)
{
    QRegularExpression re(IrGGu_IRC_Receive::removeNetwork);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString network = rem.captured("network");

        if ( networks->find(network) != networks->end() )
        {
            QMap<QString, Chat *> chats = networks->value(network)->chats;
            qDeleteAll(chats.begin(), chats.end());
            delete networks->take(network);

            Q_EMIT ircSession->networkRemoved(network);
        }
    }
}

/**
 * Handle remove chat command from the IRC client.
 *
 * @param line        Line that contains remove chat command.
 * @param *networks   List of networks that contains all IRC data.
 * @param *ircSession IRC session that received the remove chat command.
 */
void IrcSession::removeChat (QString line, NetworkList *networks, IrcSession *ircSession)
{
    QRegularExpression re(IrGGu_IRC_Receive::removeChat);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString networkName = rem.captured("network");
        Network *network    = networks->value(networkName, NULL);

        if ( network )
        {
            QString chat = rem.captured("chat");

            if ( network->chats.find(chat) != network->chats.end() )
            {
                delete network->chats.take(chat);

                Q_EMIT ircSession->chatRemoved(networkName, chat);
            }
        }
    }
}

/**
 * Handle remove nick command from the IRC client.
 *
 * @param line        Line that contains remove nick command.
 * @param *networks   List of networks that contains all IRC data.
 * @param *ircSession IRC session that received the remove nick command.
 */
void IrcSession::removeNick (QString line, NetworkList *networks, IrcSession *ircSession)
{
    QRegularExpression re(IrGGu_IRC_Receive::removeNick);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString networkName = rem.captured("network");
        Network *network    = networks->value(networkName, NULL);

        if ( network )
        {
            QString chatName = rem.captured("chat");
            Chat *chat       = network->chats.value(chatName, NULL);

            if ( chat )
            {
                QString nick = rem.captured("nick");
                int index    = chat->nickList.indexOf(nick);

                if ( index > -1 )
                {
                    chat->nickList.removeAt(index);
                }

                Q_EMIT ircSession->nickRemoved(networkName, chatName, nick);
            }
        }
    }
}

/**
 * Handle change nick command from the IRC client.
 *
 * @param line        Line that contains change nick command.
 * @param *networks   List of networks that contains all IRC data.
 * @param *ircSession IRC session that received the change nick command.
 */
void IrcSession::changeNick (QString line, NetworkList *networks, IrcSession *ircSession)
{
    QRegularExpression re(IrGGu_IRC_Receive::changeNick);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString networkName = rem.captured("network");
        Network *network    = networks->value(networkName, NULL);

        if ( network )
        {
            QString chatName = rem.captured("chat");
            Chat *chat       = network->chats.value(chatName, NULL);

            if ( chat )
            {
                QString oldNick = rem.captured("oldNick");
                QString newNick = rem.captured("newNick");
                int index       = chat->nickList.indexOf(oldNick);

                if ( index > -1 )
                {
                    chat->nickList.replace(index, newNick);
                }

                Q_EMIT ircSession->nickChanged(networkName, chatName, oldNick, newNick);
            }
        }
    }
}

/**
 * Handle change nick mode command from the IRC client.
 *
 * @param line        Line that contains change nick mode command.
 * @param *networks   List of networks that contains all IRC data.
 * @param *ircSession IRC session that received the change nick mode command.
 */
void IrcSession::changeNickMode (QString line, NetworkList *networks, IrcSession *ircSession)
{
    QRegularExpression re(IrGGu_IRC_Receive::changeNickMode);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString networkName = rem.captured("network");
        Network *network    = networks->value(networkName, NULL);

        if ( network )
        {
            QString chatName = rem.captured("chat");
            Chat    *chat    = network->chats.value(chatName, NULL);

            if ( chat )
            {
                QString nick    = rem.captured("nick");
                QString mode    = rem.captured("mode");
                QString oldNick = nick;

                if ( chat->nickList.contains("@" + nick) )
                {
                    oldNick.prepend('@');
                }
                else if ( chat->nickList.contains("%" + nick) )
                {
                    oldNick.prepend('%');
                }
                else if ( chat->nickList.contains("+" + nick) )
                {
                    oldNick.prepend('+');
                }

                int index = chat->nickList.indexOf(oldNick);

                if ( index > -1 )
                {
                    chat->nickList.replace(index, mode + nick);
                }

                Q_EMIT ircSession->nickModeChanged(networkName, chatName, oldNick, mode);
            }
        }
    }
}

/**
 * Handle rename chat command from the IRC client.
 *
 * @param line        Line that contains rename chat command.
 * @param *networks   List of networks that contains all IRC data.
 * @param *ircSession IRC session that received the rename chat command.
 */
void IrcSession::renameChat (QString line, NetworkList *networks, IrcSession *ircSession)
{
    QRegularExpression re(IrGGu_IRC_Receive::renameChat);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString networkName = rem.captured("network");
        Network *network    = networks->value(networkName, NULL);

        if ( network )
        {
            QString oldChatName = rem.captured("oldChat");
            QString newChatName = rem.captured("newChat");
            Chat    *chat       = network->chats.value(oldChatName, NULL);

            if( chat )
            {
                network->chats.remove(oldChatName);
                network->chats.insert(newChatName, chat);

                Q_EMIT ircSession->chatRenamed(networkName, oldChatName, newChatName);
            }
        }
    }
}

/**
 * Handle new message command from the IRC client.
 *
 * @param line        Line that contains new message command.
 * @param *networks   List of networks that contains all IRC data.
 * @param *ircSession IRC session that received the new message command.
 */
void IrcSession::newMsg (QString line, NetworkList *networks, IrcSession *ircSession)
{
    QRegularExpression re(IrGGu_IRC_Receive::newMsg);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString networkName = rem.captured("network");
        QString chatName    = rem.captured("chat");
        QString text        = rem.captured("msg");
        Network *network    = networks->value(networkName, NULL);

        if ( network )
        {
            Chat *chat = network->chats.value(chatName, NULL);

            if ( chat )
            {
                QMap<qint64, QString> *chatText = &chat->chat;

                if ( chatText->count() == ircSession->scrollback )
                {
                    chatText->remove(chatText->keys().first());
                    Q_EMIT ircSession->linesRemoved(networkName, chatName);
                }

                chatText->insert(Misc::getEpochNs(), text);

                Q_EMIT ircSession->msgAdded(networkName, chatName, text);
            }
            else if ( chatName == "(current)" )
            {
                Q_EMIT ircSession->msgAdded(networkName, chatName, text);
            }
        }
        else if ( networkName == "(current)" || networkName == "(none)" )
        {
            Q_EMIT ircSession->msgAdded(networkName, chatName, text);
        }
    }
}

/**
 * Handle new highlight message command from the IRC client.
 *
 * @param line        Line that contains new highlight message command.
 * @param *networks   List of networks that contains all IRC data.
 * @param *ircSession IRC session that received the new highlight message command.
 */
void IrcSession::newHighlightMsg (QString line, NetworkList *networks, IrcSession *ircSession)
{
    QRegularExpression re(IrGGu_IRC_Receive::newHighlightMsg);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString networkName = rem.captured("network");
        Network *network    = networks->value(networkName, NULL);

        if ( network )
        {
            QString chatName = rem.captured("chat");
            QString text     = rem.captured("msg");
            Chat    *chat    = network->chats.value(chatName, NULL);

            if ( chat )
            {
                QMap<qint64, QString> *chatText = &chat->chat;

                if ( chatText->count() == ircSession->scrollback )
                {
                    chatText->remove(chatText->keys().first());
                    Q_EMIT ircSession->linesRemoved(networkName, chatName);
                }

                chatText->insert(Misc::getEpochNs(), text);

                Q_EMIT ircSession->highlightMsgAdded(networkName, chatName, text);
            }
        }
    }
}

/**
 * Handle new own message command from the IRC client.
 *
 * @param line        Line that contains new own message command.
 * @param *networks   List of networks that contains all IRC data.
 * @param *ircSession IRC session that received the new own message command.
 */
void IrcSession::newOwnMsg (QString line, NetworkList *networks, IrcSession *ircSession)
{
    QRegularExpression re(IrGGu_IRC_Receive::newOwnMsg);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString networkName = rem.captured("network");
        Network *network    = networks->value(networkName, NULL);

        if ( network )
        {
            QString chatName = rem.captured("chat");
            QString text     = rem.captured("msg");
            Chat *chat       = network->chats.value(chatName, NULL);

            if ( chat )
            {
                QMap<qint64, QString> *chatText = &chat->chat;

                if ( chatText->count() == ircSession->scrollback )
                {
                    chatText->remove(chatText->keys().first());
                    Q_EMIT ircSession->linesRemoved(networkName, chatName);
                }

                chatText->insert(Misc::getEpochNs(), text);

                Q_EMIT ircSession->ownMsgAdded(networkName, chatName, text);
            }
        }
    }
}

/**
 * Handle new DCC file command from the IRC client.
 *
 * @param line        Line that contains new DCC file command.
 * @param *networks   List of networks that contains all IRC data.
 * @param *ircSession IRC session that received the new DCC file command.
 */
void IrcSession::newDccFile (QString line, NetworkList *networks, IrcSession *ircSession)
{
    QRegularExpression re(IrGGu_IRC_Receive::newDccFile);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString networkName = rem.captured("network");
        QString sender      = rem.captured("sender");
        QString filename    = rem.captured("file");
        Network *network    = networks->value(networkName, NULL);

        if ( network )
        {
            Q_EMIT ircSession->incommingDccFile(networkName, sender, filename);
        }
    }
}

/**
 * Handle close DCC file command from the IRC client.
 *
 * @param line        Line that contains close DCC file command.
 * @param *networks   List of networks that contains all IRC data.
 * @param *ircSession IRC session that received the close DCC file command.
 */
void IrcSession::closeDccFile (QString line, NetworkList *networks, IrcSession *ircSession)
{
    QRegularExpression re(IrGGu_IRC_Receive::closeDccFile);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString networkName = rem.captured("network");
        QString sender      = rem.captured("sender");
        QString filename    = rem.captured("file");
        Network *network    = networks->value(networkName, NULL);

        if ( network )
        {
            Q_EMIT ircSession->closeDccFile(networkName, sender, filename);
        }
    }
}

/**
 * Handle line completed command from the IRC client.
 *
 * @param line        Line that contains line completed command.
 * @param *networks   List of networks that contains all IRC data.
 * @param *ircSession IRC session that received the line completed command.
 */
void IrcSession::lineCompleted (QString line, NetworkList *networks, IrcSession *ircSession)
{
    QRegularExpression re(IrGGu_IRC_Receive::lineCompleted);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString networkName = rem.captured("network");
        QString chatName    = rem.captured("chat");
        QString text        = rem.captured("line");
        Network *network    = networks->value(networkName, NULL);

        if ( network )
        {
            Chat *chat = network->chats.value(chatName, NULL);

            if ( chat && ircSession->completeLineReplyTarget )
            {
                ircSession->completeLineReplyTarget->completedLine(networkName, chatName, text);
            }
        }
        else if ( networkName == "(none)" && ircSession->completeLineReplyTarget )
        {
            ircSession->completeLineReplyTarget->completedLine(networkName, chatName, text);
        }

        ircSession->completeLineReplyTarget = NULL;
    }
}

/**
 * Called when server configuration has been changed.
 */
void IrcSession::settingsChanged ()
{
    int scrollback = Config::getConfig()->scrollback;

    if ( this->scrollback > scrollback )
    {
        setScrollback(scrollback);
    }
}

/**
 * Read data sent by IRC client.
 */
void IrcSession::readData ()
{
    QString line;
    QString command;

    while ( socket->canReadLine() )
    {
        line = QString::fromUtf8(socket->readLine().trimmed());

        QRegularExpression commandRe(IrGGu_IRC_Commands::regex);
        QRegularExpressionMatch commandRem = commandRe.match(line);

        command = commandRem.captured("command");

        if ( !isRegistered && command != IrGGu_IRC_Commands::registerIRC
             && command != IrGGu_IRC_Commands::registerForceIRC
             && command != IrGGu_IRC_Commands::newSettingValue
             && command != IrGGu_IRC_Commands::setColors )
        {

            QString log = "User " + QString::number(socket->getUid()) + " send command " + command
                          + ", before successfully register! Closing connection!";

            Misc::log(log);
            socket->disconnectFromClient();
        }
        else if ( !isRegistered && command == IrGGu_IRC_Commands::registerIRC )
        {
            registerSession(line);
        }
        else if ( !isRegistered && command == IrGGu_IRC_Commands::registerForceIRC )
        {
            registerSession(line, true);
        }
        else
        {
            IrcSessionHandleMethod *method = handleMethods.value(command, NULL);

            if ( method )
            {
                method(line, &networks, this);
            }
        }
    }
}

/**
 * Called when socket has disconnected.
 */
void IrcSession::disconnected ()
{
    Manager::deleteAccount(uid);

    this->deleteLater();
}

/**
 * Called when data has been sent to client.
 */
void IrcSession::bytesWritten ()
{
    if ( socket->bytesToWrite() == 0 )
    {
        socket->disconnectFromClient();
    }
}

/**
 * Write line to IRC.
 *
 * @param network Network.
 * @param chat    Chat.
 * @param line    Line.
 */
void IrcSession::writeLine (QString network, QString chat, QString line)
{
    QString command = IrGGu_IRC_Write::writeLine;

    command.replace("<network>", network);
    command.replace("<chat>", chat);
    command.replace("<line>", line);

    write(command);
}

/**
 * Write complete line command to IRC client.
 *
 * @param network Network.
 * @param chat    Chat.
 * @param line    Line.
 * @param pos     Cursor position.
 */
void IrcSession::completeLine (QString network, QString chat, QString line, int pos)
{
    completeLineReplyTarget = static_cast<ClientSession *>(this->sender());

    QString command = IrGGu_IRC_Write::completeLine;

    command.replace("<network>", network);
    command.replace("<chat>", chat);
    command.replace("<pos>", QString::number(pos));
    command.replace("<line>", line);

    write(command);
}

/**
 * Write dcc file reply to IRC client.
 *
 * @param network Network.
 * @param chat    Chat.
 * @param file    File.
 * @param get     If true get file.
 */
void IrcSession::dccFileReply (QString network, QString sender, QString file, bool get)
{
    QString command = get ? IrGGu_IRC_Write::acceptDccFile : IrGGu_IRC_Write::doNotAcceptDccFile;

    command.replace("<network>", network);
    command.replace("<sender>", sender);
    command.replace("<file>", file);

    write(command);
}

/**
 * Write quit network command to IRC client.
 *
 * @param network Network.
 */
void IrcSession::quitNetwork (QString network)
{
    QString command = IrGGu_IRC_Write::quitNetwork;

    command.replace("<network>", network);

    write(command);
}

/**
 * Write close chat command to IRC client.
 *
 * @param network Network.
 * @param chat    Chat.
 */
void IrcSession::closeChat (QString network, QString chat)
{
    QString command = IrGGu_IRC_Write::closeChat;

    command.replace("<network>", network);
    command.replace("<chat>", chat);

    write(command);
}

/**
 * Write query command to IRC client.
 *
 * @param network Network.
 * @param nick    Nick.
 */
void IrcSession::query (QString network, QString nick)
{
    QString command = IrGGu_IRC_Write::query;

    command.replace("<network>", network);
    command.replace("<nick>", nick);

    write(command);
}

/**
 * Signal is emitted when network has been added.
 *
 * @fn void IrcSession::networkAdded(QString network)
 * @param network Network name.
 */

/**
 * Signal is emitted when chat has been added.
 *
 * @fn void IrcSession::chatAdded(QString network, QString chat)
 * @param network Network name.
 * @param chat    Chat name.
 */

/**
 * Signal is emitted when nick has been added.
 *
 * @fn void IrcSession::nickAdded(QString network, QString chat, QString nick)
 * @param network Network name.
 * @param chat    Chat name.
 * @param nick    Nick.
 */

/**
 * Signal is emitted when nicks has been added.
 *
 * @fn void IrcSession::nicksAdded(QString network, QString chat, QStringList nicks)
 * @param network Network name.
 * @param chat    Chat name.
 * @param nicks   Nicks.
 */

/**
 * Signal is emitted when network has been removed.
 *
 * @fn void IrcSession::networkRemoved(QString network)
 * @param network Network name.
 */

/**
 * Signal is emitted when chat has been removed.
 *
 * @fn void IrcSession::chatRemoved(QString network, QString chat)
 * @param network Network name.
 * @param chat    Chat name.
 */

/**
 * Signal is emitted when nick has been removed.
 *
 * @fn void IrcSession::nickRemoved(QString network, QString chat, QString nick)
 * @param network Network name.
 * @param chat    Chat name.
 * @param nick    Nick.
 */


/**
 * Signal is emitted when nick has been changed.
 *
 * @fn void IrcSession::nickChanged(QString network, QString chat, QString oldNick, QString newNick)
 * @param network Network name.
 * @param chat    Chat name.
 * @param oldNick Old nick.
 * @param newNick New nick.
 */

/**
 * Signal is emitted when nick mode has been changed.
 *
 * @fn void IrcSession::nickModeChanged(QString network, QString chat, QString nick, QString mode)
 * @param network Network name.
 * @param chat    Chat name.
 * @param nick    Nick.
 * @param mode    New mode.
 */

/**
 * Signal is emitted when chat has been renamed.
 *
 * @fn void IrcSession::chatRenamed(QString network, QString oldChatName, QString newChatName)
 * @param network Network name.
 * @param oldChat Old chat name.
 * @param newChat New chat name.
 */

/**
 * Signal is emitted when new message has been added.
 *
 * @fn void IrcSession::msgAdded(QString network, QString chat, QString line)
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Chat line.
 */

/**
 * Signal is emitted when new highlight message has been added.
 *
 * @fn void IrcSession::highlightMsgAdded(QString network, QString chat, QString line)
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Chat line.
 */


/**
 * Signal is emitted when new own message has been added.
 *
 * @fn void IrcSession::ownMsgAdded(QString network, QString chat, QString line)
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Chat line.
 */

/**
 * Signal is emitted when there is new incomming DCC file.
 *
 * @fn void IrcSession::incommingDccFile(QString network, QString sender, QString file)
 * @param network Network name.
 * @param sender  Sender's name.
 * @param file    Filename.
 */

/**
 * Signal is emitted when incomming DCC file request has been closed.
 *
 * @fn void IrcSession::closeDccFile(QString network, QString sender, QString file)
 * @param network Network name.
 * @param sender  Sender's name.
 * @param file    Filename.
 */

/**
 * Signal is emitted when lines has been removed.
 *
 * @fn void IrcSession::linesRemoved(QString network, QString chat)
 * @param network Network name.
 * @param chat    Chat name.
 */
