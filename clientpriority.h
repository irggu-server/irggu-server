/****************************************************************************
**  clientpriority.h
**
**  Copyright information
**
**      Copyright (C) 2012-2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef CLIENTPRIORITY_H
#define CLIENTPRIORITY_H

#include <QObject>
#include <QList>
#include <QMap>

class ClientSession;

/**
 *  This class is for handling client priority.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2013-08-25
 */
class ClientPriority : public QObject
{
    Q_OBJECT
public:
    ClientPriority();

    void addClientSession(ClientSession *clientSession);
    void removeClientSession(ClientSession *clientSession);
    void setIdle(bool idle, int priority, ClientSession *clientSession);
    bool isHighPriority(ClientSession *clientSession);

private:
    QMap<ClientSession*, int> clients;
    QList<ClientSession*> idlers;
    int priority;
};

#endif // CLIENTPRIORITY_H
