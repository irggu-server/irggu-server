/****************************************************************************
**  manager.cpp
**
**  Copyright information
**
**      Copyright (C) 2011-2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "protocol/irggu.h"
#include "manager.h"
#include "misc.h"
#include "config.h"
#include <QtCore/QCryptographicHash>
#include <QtCore/QByteArray>

QMap<int, AccountData*>     Manager::users;
QMap<QString, AccountData*> Manager::accounts;
int                         Manager::maxClientSessions;

/**
 * Set.
 */
void Manager::set ()
{
    maxClientSessions = Config::getConfig()->maxClientSessions;
}

/**
 * Register account.
 *
 * @param uid         User uid.
 * @param username    Username.
 * @param password    Password.
 * @param *ircSession IRC session.
 * @param force       If true and registered then delete existing account and register new.
 * @return Result of registering.
 */
int Manager::registerAccount (int uid, QString username, QString password,
                              IrcSession *ircSession, bool force)
{
    int error         = IrGGu_IRC_Register::noError;
    bool isRegistered = users.find(uid) != users.end();

    if ( !isRegistered || force )
    {
        if ( isRegistered )
        {
            deleteAccount(uid);
        }

        if ( accounts.find(username) == accounts.end() )
        {
            AccountData *account = new AccountData;
            account->uid         = uid;
            account->password    = password;
            account->ircSession  = ircSession;
            users.insert(uid, account);
            accounts.insert(username, account);
        }
        else
        {
            error = IrGGu_IRC_Register::usernameInUse;
        }
    }
    else
    {
        error = IrGGu_IRC_Register::alreadyRegistered;
    }

    return error;
}

/**
 * Handle login.
 *
 * @param username     Username.
 * @param password     Password.
 * @param *socket      Socket.
 * @param sessionID    Session ID.
 * @param connectionID Connection ID.
 * @return Result of login.
 */
int Manager::login (QString username, QString password, TcpSocket *socket, int sessionID,
                    int connectionID)
{
    int error = IrGGu_Login::noError;

    if ( accounts.find(username) != accounts.end() )
    {
        AccountData *account = accounts.value(username);
        QByteArray hash      = QCryptographicHash::hash(password.toUtf8(),
                                                        QCryptographicHash::Md5);
        password             = QString::fromUtf8(hash.toHex());

        if ( account->password == password )
        {
            QString log = "User " + QString::number(account->uid) + " authenticated from "
                          + socket->clientAddress().toString();

            Misc::log(log);

            bool hasSession = false;

            ClientSession *session;

            if ( sessionID != 0 )
            {
                session = account->clientSessions.value(sessionID, NULL);

                if ( session )
                {
                    if ( session->isSameClient(connectionID) )
                    {
                        hasSession = true;
                    }
                }
            }

            if ( !hasSession )
            {
                bool canCreateNewSession = true;

                if ( account->clientSessions.size() == maxClientSessions )
                {
                    QMapIterator<int, ClientSession*> clientSessionsIt(account->clientSessions);
                    ClientSession *clientSession;
                    qint64 epochNow        = Misc::getEpochNs();
                    qint64 epoch           = epochNow;
                    qint64 disconnectEpoch = 0;
                    int sessionId          = 0;

                    while ( clientSessionsIt.hasNext() )
                    {
                        clientSessionsIt.next();
                        clientSession   = clientSessionsIt.value();
                        disconnectEpoch = clientSession->getLastDisconnected();

                        if ( disconnectEpoch && disconnectEpoch < epoch )
                        {
                            epoch     = disconnectEpoch;
                            sessionId = clientSessionsIt.key();
                        }
                    }

                    if ( epochNow != epoch )
                    {
                        exemptSessionId(username, sessionId);
                    }
                    else
                    {
                        canCreateNewSession = false;
                        error               = IrGGu_Login::maxClientLimit;
                    }
                }

                if ( canCreateNewSession )
                {
                    if ( !account->exemptedSessionIds.isEmpty() )
                    {
                        sessionID = account->exemptedSessionIds.takeAt(0);
                    }
                    else
                    {
                        if ( !account->clientSessions.isEmpty() )
                        {
                            sessionID = account->clientSessions.keys().last() + 1;
                        }
                        else
                        {
                            sessionID = 1;
                        }
                    }

                    session = new ClientSession(username, sessionID);
                    account->clientSessions.insert(sessionID, session);
                    account->ircSession->insertClientSession(session);
                }
                else
                {
                    session = NULL;
                }
            }

            if ( session )
            {
                session->setSocket(socket);
            }
        }
        else
        {
            error = IrGGu_Login::wrongUserPass;
        }
    }
    else
    {
        error = IrGGu_Login::wrongUserPass;
    }

    return error;
}

/**
 * Delete account.
 *
 * @param uid.
 */
void Manager::deleteAccount (int uid)
{
    if ( users.find(uid) != users.end() )
    {
        AccountData *account = users.take(uid);
        QString username     = accounts.key(account);

        accounts.remove(username);
        delete account;
    }
}

/**
 * Delete all accounts.
 */
void  Manager::deleteAllAccounts ()
{
    QListIterator<AccountData*> accountsIt(users.values());

    while( accountsIt.hasNext() )
    {
        delete accountsIt.next();
    }

    users.clear();
}

/**
 * Exempt session ID.
 *
 * @param username Username.
 * @param id       Session ID.
 */
void Manager::exemptSessionId (QString username, int id)
{
    if ( accounts.find(username) != accounts.end() )
    {
        AccountData *account   = accounts.value(username);

        if ( account->clientSessions.find(id) != account->clientSessions.end() )
        {
            delete account->clientSessions.take(id);
            account->exemptedSessionIds.append(id);
        }
    }
}
