/****************************************************************************
**  config.h
**
**  Copyright information
**
**      Copyright (C) 2011-2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Server.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef CONFIG_H
#define CONFIG_H

#include <QtCore/QMap>
#include <QtCore/QStringList>

struct ConfigStruct {
    int     securePort;
    int     insecurePort;
    int     scrollback;
    int     maxClientSessions;
    QString privateKey;
    QString certificate;
};

/**
 *  This class is for server configuration.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2013-27-07
 */
class Config
{
public:
    static void set();
    static ConfigStruct *getConfig();

private:
    static ConfigStruct config;

    static void readConfig();
};

#endif // CONFIG_H
